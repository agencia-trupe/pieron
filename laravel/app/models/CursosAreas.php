<?php

class CursosAreas extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cursos_areas';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    public function scopeOrdenado($query)
    {
    	return $query->orderBy('ordem', 'asc')->get();
    }

    public function cursosPorNome(){
    	return $this->hasMany('Cursos', 'cursos_areas_id')->orderBy(Lang::get('base.campos.titulo'), 'asc');
    }

    public function cursosPorData()
    {
    	return $this->hasMany('Cursos', 'cursos_areas_id')
    				->leftJoin('curso_unidade', 'cursos.id', '=', 'curso_unidade.curso_id')
                    ->select(DB::raw('cursos.*, curso_unidade.*, MIN(curso_unidade.data_inicio) as maxdata'))
                    ->orderBy('maxdata', 'asc');
    }

    // Método novo - retorna todas as turmas do curso ordenadas por data - mostrar tudo
    public function cursosOrdenadoPorData()
    {
        $query = <<<STR
SELECT
	cursos_areas.titulo as areatitulo,
	cursos.id as cursoid,
	cursos.titulo as cursotitulo,
	cursos.slug as cursoslug,
	turmas.data_inicio as turmasdata,
	turmas.unidade_id as turmaunidade,
	unidades.cidade as unidadecidade
FROM cursos_areas
LEFT JOIN cursos ON cursos_areas.id = cursos.cursos_areas_id
LEFT JOIN curso_unidade turmas ON cursos.id = turmas.curso_id
LEFT JOIN unidades ON turmas.unidade_id = unidades.id
WHERE cursos_areas.id = {$this->id}
ORDER BY CASE WHEN turmasdata IS NULL OR turmasdata = '0000-00-00' THEN 1 ELSE 0 END, turmasdata ASC
STR;
        return DB::select(DB::raw($query));
    }

//	Método para retornar os cursos da área começando pela data de turma mais próxima pra cada um - para mostrar só a 1a turma
//  public function cursosOrdenadoPorData()
//     {
//         $query = <<<STR
// SELECT *
// FROM(
//     SELECT
//     cursos_areas.titulo as areatitulo,
//     cursos.id as cursoid,
//     cursos.titulo as cursotitulo,
//     cursos.slug as cursoslug,
//     turmas.data_inicio as turmasdata,
//     turmas.unidade_id as turmaunidade,
//     unidades.cidade as unidadecidade
//     from cursos_areas
//     LEFT JOIN cursos ON cursos_areas.id = cursos.cursos_areas_id
//     LEFT JOIN curso_unidade turmas ON cursos.id = turmas.curso_id
//     LEFT JOIN unidades ON turmas.unidade_id = unidades.id
//     WHERE cursos_areas.id = {$this->id}
//     ORDER BY CASE WHEN turmasdata IS NULL THEN 1 ELSE 0 END, turmasdata ASC
// ) as _t
// GROUP BY _t.cursoid 
// ORDER BY CASE WHEN _t.turmasdata IS NULL THEN 1 ELSE 0 END, _t.turmasdata ASC;
// STR;
//         return DB::select(DB::raw($query));
//     }


}