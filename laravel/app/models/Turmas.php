<?php

class Turmas extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'curso_unidade';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    public function unidade()
    {
    	return $this->belongsTo('Unidade', 'unidade_id');    	
    }

    public function scopeOrdenado($query, $ordenacao = 'data')
    {
    	$ord = ($ordenacao == 'data') ? 'data_inicio' : 'data_inicio';
    	return $query->orderBy($ord, 'asc')->get();
    }
}