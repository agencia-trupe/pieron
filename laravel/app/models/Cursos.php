<?php

class Cursos extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cursos';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    public function scopeOrdenado($query)
    {
    	return $query->orderBy('ordem', 'asc')->get();
    }

    public function area(){
    	return $this->belongsTo('CursosAreas', 'cursos_areas_id');
    }

    public function unidades(){
    	return $this->orderBy('ordem', 'asc')->belongsToMany('Unidade', 'unidade_id');
    }

    public function turmas()
    {
    	return $this->hasMany('Turmas', 'curso_id')->orderBy('data_inicio', 'asc');
    }

}