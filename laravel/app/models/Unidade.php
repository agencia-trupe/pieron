<?php

class Unidade extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'unidades';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    public function scopeOrdenado($query)
    {
    	return $query->orderBy('ordem', 'asc')->get();
    }

    public function cursos(){
    	return $this->orderBy('ordem', 'asc')->belongsToMany('Cursos', 'curso_id');
    }

}