<?php

use \OPieron, \ImagensTopo, \Equipe, \Historia;

class OPieronController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index($slug_texto = false)
	{

		if(!$slug_texto)
			$detalhe = OPieron::orderBy('ordem', 'asc')->first();
		else{
			$detalhe = OPieron::where('slug', '=', $slug_texto)->first();

			if(is_null($detalhe)){
				App::abort('404');
			}
		}

		$this->layout->with('marcarSub', $detalhe->{Lang::get('base.campos.slug')});
		$this->layout->content = View::make('frontend.opieron.index')->with('textos', OPieron::ordenado())
																	 ->with('detalhe', $detalhe)
																	 ->with('slugFiltro', $detalhe->{Lang::get('base.campos.slug')})
																	 ->with('listaHistoria', Historia::ordenado())
																	 ->with('listaEquipe', Equipe::ordenado())
																	 ->nest('formulario', 'frontend.partials.formulario')
																	 ->nest('topo', 'frontend.partials.topo', array(
																	 		'bannerTopo' => ImagensTopo::where('slug', '=', 'opieron')->first(),
																	 		'textoArea' => Lang::get('base.secoes.opieron')
																	 ));
	}

}
