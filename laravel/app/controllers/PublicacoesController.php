<?php

use \Clipping, \Artigo;

class PublicacoesController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function artigos()
	{
		$view = 'frontend.publicacoes.artigos.index';
		$this->layout->with('css', 'css/publicacoes');
		$this->layout->content = View::make($view)->with('artigos', Artigo::ordenado()->paginate(6))
											      ->with('slugFiltro', 'artigos')
											      ->nest('formulario', 'frontend.partials.formulario')
											      ->nest('topo', 'frontend.partials.topo', array(
													'bannerTopo' => ImagensTopo::where('slug', '=', 'publicacoes')->first(),
													'textoArea' => Lang::get('base.secoes.publicacoes')
											       ));
	}

	public function artigosDetalhes($slug = '')
	{
		if(!$slug) return Redirect::route('publicacoes.artigos');

		$detalhes = \Artigo::where('slug', '=', $slug)->first();

		if(!$detalhes) return Redirect::route('publicacoes.artigos');

		$previousArtigo = Artigo::where('data', '<=', $detalhes->data)
								->where('id', '!=', $detalhes->id)
								->orderBy('data', 'DESC')
								->orderBy('id', 'ASC')
								->first();

		$nextArtigo = Artigo::where('data', '>=', $detalhes->data)
							->where('id', '!=', $detalhes->id)
							->orderBy('data', 'ASC')
							->orderBy('id', 'ASC')
							->first();

		$view = 'frontend.publicacoes.artigos.detalhes';
		$this->layout->with('css', 'css/publicacoes');
		$this->layout->content = View::make($view)->with('artigos', Artigo::ordenado()->where('id', '!=', $detalhes->id)->paginate(5))
											      ->with('slugFiltro', 'artigos')
											      ->with('detalhe', $detalhes)
											      ->with('previousArtigo', $previousArtigo)
											      ->with('nextArtigo', $nextArtigo)
											      ->nest('formulario', 'frontend.partials.formulario')
											      ->nest('topo', 'frontend.partials.topo', array(
													'bannerTopo' => ImagensTopo::where('slug', '=', 'publicacoes')->first(),
													'textoArea' => Lang::get('base.secoes.publicacoes')
											       ));	
	}

	public function clipping()
	{
		$view = 'frontend.publicacoes.clipping.index';
		$this->layout->with('css', 'css/publicacoes');
		$this->layout->content = View::make($view)->with('clippings', Clipping::ordenado()->paginate(15))
												  ->with('slugFiltro', 'clipping')
												  ->nest('formulario', 'frontend.partials.formulario')
											      ->nest('topo', 'frontend.partials.topo', array(
													'bannerTopo' => ImagensTopo::where('slug', '=', 'publicacoes')->first(),
													'textoArea' => Lang::get('base.secoes.publicacoes')
											       ));	
	}

	public function galeria($slug_galeria = '')
	{
		$clip = Clipping::where('slug', '=', $slug_galeria)->first();
		if(!$clip) return false;
		return Response::view('frontend.publicacoes.clipping.galeria', array('clip' => $clip));
	}

	public function download($slug = '')
	{
		if(!$slug) return Redirect::route('publicacoes.artigos');

		$detalhes = \Artigo::where('slug', '=', $slug)->first();

		if(!$detalhes) return Redirect::route('publicacoes.artigos');

        $file = public_path(). "/assets/files/artigos/".$detalhes->pdf;
        $headers = array(
        	'Content-Type: application/pdf',
        );
        return Response::download($file, $detalhes->pdf, $headers);
	}

}
