<?php

use \banner, \Chamada, \OPieron, \AreasAtuacao, \Unidade, \Cursos, \CursosAreas;

class HomeController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.home.index')->with('banners', banner::ordenado())
																  ->with('chamadas', Chamada::ordenado());
	}

	public function cadastro()
	{
		$nome = Input::get('nome');
		$email = Input::get('email');

		if(sizeof(Cadastro::where('email', '=', $email)->first()))
		{
			return Lang::get('base.geral.emailJaCadastrado');
		}
		else
		{
			$novo = new Cadastro;
			$novo->nome = $nome;
			$novo->email = $email;
			$novo->save();
			return Lang::get('base.geral.cadastroSucesso');
		}
	}

	public function contato()
	{
/*
Consultoria => atuacao/consultoria
Assessment estratégico => atuacao/assessment-estrategico
Desenvolvimento => atuacao/desenvolvimento
Análise de perfil => atuacao/analise-de-perfil
Coaching Flow => atuacao/coaching-flow
Plataforma Insights Discovery => atuacao/plataforma-insights-discoveryr

Cursos & Treinamentos => cursos/*

Venda de materiais => materiais

Outros => resto
*/

		$areas = array(
			'opieron' => 'O Pieron',
			'atuacao' => 'Área de Atuação',
			'flowconditions' => 'Flow Conditions System®',
			'materiais' => 'Venda de Materiais',
			'cursos' => 'Cursos',
			'cursos.areas' => 'Cursos da Área',
			'cursos.detalhes' => 'Curso',
		);

		$data['nome'] = Input::get('nome');
		$data['email'] = Input::get('email');
		$data['telefone'] = Input::get('telefone');
		$data['empresa'] = Input::get('empresa');
		$data['assunto'] = Input::get('assunto');
		$data['mensagem'] = Input::get('mensagem');
		$data['origem'] = Input::get('origem');

		//Teste de envio pela página de contato
		$xpd = explode('/', $data['origem']);
		$data['slug'] = end($xpd);
		$data['rota'] = Input::get('rota');


		// Definição de Assunto
		if(!$data['assunto']){

			if($data['rota'] == 'atuacao'){

				$xpd = explode('/', $data['origem']);
				$data['slug'] = end($xpd);
				$data['subject'] = sizeof(AreasAtuacao::where('slug', '=', $data['slug'])->get()) > 0 ? AreasAtuacao::where('slug', '=', $data['slug'])->first()->titulo : "Atuação";
			
			}elseif(str_is('cursos*', $data['rota'])){

				$data['subject'] = "Cursos & Treinamentos";
			
			}elseif(str_is('materiais*', $data['rota'])){
				$data['subject'] = "Venda de materiais";
			}else{
				$data['subject'] = "Outros";
			}
		}else{
			$data['subject'] = $data['assunto'];
		}

		// Definição de destinatários
		if(str_is('Consultoria*', $data['subject'])){
			$data['destinos'] = array("relacionamento@pieron.com.br", "rafaelbruno@pieron.com.br", "marcosbruno@pieron.com.br");
		}elseif(str_is('Assessment*', $data['subject'])){
			$data['destinos'] = array("relacionamento@pieron.com.br", "rafaelbruno@pieron.com.br", "marcosbruno@pieron.com.br");
		}elseif(str_is('Desenvolvimento*', $data['subject'])){
			$data['destinos'] = array("relacionamento@pieron.com.br", "rafaelbruno@pieron.com.br", "marcosbruno@pieron.com.br");
		}elseif(str_is('Análise*', $data['subject'])){
			$data['destinos'] = array("relacionamento@pieron.com.br", "rafaelbruno@pieron.com.br", "danielabalboa@pieron.com.br");
		}elseif(str_is('Coaching*', $data['subject'])){
			$data['destinos'] = array("relacionamento@pieron.com.br", "rafaelbruno@pieron.com.br", "marcosbruno@pieron.com.br");
		}elseif(str_is('Plataforma*', $data['subject'])){
			$data['destinos'] = array("relacionamento@pieron.com.br", "rafaelbruno@pieron.com.br", "flavialima@pieron.com.br", "willianbull@pieron.com.br");
		}elseif(str_is('Cursos*', $data['subject'])){
			$data['destinos'] = array("relacionamento@pieron.com.br", "rafaelbruno@pieron.com.br", "cursos@pieron.com.br");
		}elseif(str_is('Venda*', $data['subject'])){
			$data['destinos'] = array("relacionamento@pieron.com.br", "rafaelbruno@pieron.com.br");
		}else{
			$data['destinos'] = array("relacionamento@pieron.com.br", "rafaelbruno@pieron.com.br");
		}


		if($data['nome'] && $data['email']){
			Mail::send('emails.contato', $data, function($message) use ($data)
			{
				if(is_array($data['destinos'])){
					foreach ($data['destinos'] as $key => $value) {
						$message->to($value);						
					}
				}else{
			    	$message->to('relacionamento@pieron.com.br', 'Instituto Pieron')
			    			->to('rafaelbruno@pieron.com.br', 'Rafael Beran Bruno');
				}
	    		$message->subject($data['subject'])
	    				->replyTo($data['email'], $data['nome'])
	    				->to('instituto@pieron.com.br');

	    		$headers = $message->getHeaders();
    			$headers->addTextHeader('X-MC-PreserveRecipients', 'false');
	    		if(Config::get('mail.pretend')){
	    			Log::info(View::make('emails.contato',$data)->render());
	    			Log::info($message->getHeaders());
	    		}
			});

			$contato = new \Contato;
			$contato->assunto = htmlspecialchars($data['subject']);
			$contato->nome = htmlspecialchars($data['nome']);
			$contato->email = htmlspecialchars($data['email']);
			$contato->telefone = htmlspecialchars($data['telefone']);
			$contato->empresa = htmlspecialchars($data['empresa']);
			$contato->mensagem = htmlspecialchars($data['mensagem']);
			$contato->origem = htmlspecialchars($data['origem']);
			$contato->save();
		}
		
		return Lang::get('base.geral.contatoEnviado');
	}

}
