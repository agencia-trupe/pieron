<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, CursosAreas;

class CursosAreasController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.cursosareas.index')->with('registros', CursosAreas::orderBy('ordem', 'ASC')->get());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.cursosareas.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new CursosAreas;

		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));

		if(sizeof(CursosAreas::where('slug', '=', $object->slug)->get()))
			return Redirect::back()->withErrors(array('Este título já está sendo utilizado!'));	

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Área criada com sucesso.');
			return Redirect::route('painel.cursosareas.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Área!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.cursosareas.edit')->with('registro', CursosAreas::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = CursosAreas::find($id);

		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));

		if(sizeof(CursosAreas::where('slug', '=', $object->slug)->where('id', '!=', $object->id)->get()))
			return Redirect::back()->withErrors(array('Este título já está sendo utilizado!'));	
	
		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Área alterada com sucesso.');
			return Redirect::route('painel.cursosareas.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Área!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = CursosAreas::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Área removida com sucesso.');

		return Redirect::route('painel.cursosareas.index');
	}

}