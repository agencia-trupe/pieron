<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \Turmas, \CursosAreas, \Cursos, \Unidade;

class TurmasController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(Input::has('unidade')){
			$unidade = Input::get('unidade');
			$turmas = Turmas::where('unidade_id', '=', $unidade)->orderBy('data_inicio')->paginate(25);
		}else{
			$unidade = '';
			$turmas = Turmas::orderBy('data_inicio')->paginate(25);
		}
		$this->layout->content = View::make('backend.turmas.index')->with('registros', $turmas)
																   ->with('filtro', $unidade);		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.turmas.form')->with('listaCursos', Cursos::ordenado())
																  ->with('listaAreas', CursosAreas::ordenado())
																  ->with('listaUnidades', Unidade::ordenado());
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Turmas;

		$object->data_inicio = Tools::converteData(Input::get('data_inicio'));
		$object->cronograma = Input::get('cronograma');
		$object->curso_id = Input::get('curso_id');
		$object->unidade_id = Input::get('unidade_id');

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Turma criada com sucesso.');
			return Redirect::route('painel.turmas.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Turma!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.turmas.edit')->with('registro', Turmas::find($id))
																  ->with('listaAreas', CursosAreas::ordenado())
																  ->with('listaCursos', Cursos::ordenado())
																  ->with('listaUnidades', Unidade::ordenado());
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Turmas::find($id);

		$object->data_inicio = Tools::converteData(Input::get('data_inicio'));
		$object->cronograma = Input::get('cronograma');
		$object->curso_id = Input::get('curso_id');
		$object->unidade_id = Input::get('unidade_id');

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Turma alterada com sucesso.');
			return Redirect::route('painel.turmas.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Turma!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Turmas::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Turma removida com sucesso.');

		return Redirect::route('painel.turmas.index');
	}

}