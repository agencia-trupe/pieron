<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \Cursos, \CursosAreas;

class CursosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$filtroArea = Input::get('cursos_areas_id');

		$cursos = Cursos::where('cursos_areas_id', '=', $filtroArea)->orderBy('ordem', 'ASC')->get();

		$this->layout->content = View::make('backend.cursos.index')->with('registros', $cursos)																   
																   ->with('listaAreas', CursosAreas::ordenado())
																   ->with('filtroArea', $filtroArea);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.cursos.form')->with('listaAreas', CursosAreas::ordenado());
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Cursos;

		$object->cursos_areas_id = Input::get('cursos_areas_id');
		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));
		$object->objetivos = Input::get('objetivos');
		$object->publico_alvo = Input::get('publico_alvo');
		$object->metodologia = Input::get('metodologia');
		$object->conteudo = Input::get('conteudo');
		$object->carga_horaria = Input::get('carga_horaria');
		$object->valor = Input::get('valor');
		$object->formas_pagamento = Input::get('formas_pagamento');
		$object->docentes = Input::get('docentes');
		$object->pre_requisitos = Input::get('pre_requisitos');
		$object->documentacao = Input::get('documentacao');
		$object->observacoes = Input::get('observacoes');

		if(sizeof(Cursos::where('slug', '=', $object->slug)->get()))
			return Redirect::back()->withErrors(array('Este título já está sendo utilizado!'));	

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Curso criado com sucesso.');
			return Redirect::route('painel.cursos.index', array('cursos_areas_id' => $object->cursos_areas_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Curso!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.cursos.edit')->with('registro', Cursos::find($id))->with('listaAreas', CursosAreas::ordenado());
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Cursos::find($id);

		$object->cursos_areas_id = Input::get('cursos_areas_id');
		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));
		$object->objetivos = Input::get('objetivos');
		$object->publico_alvo = Input::get('publico_alvo');
		$object->metodologia = Input::get('metodologia');
		$object->conteudo = Input::get('conteudo');
		$object->carga_horaria = Input::get('carga_horaria');
		$object->valor = Input::get('valor');
		$object->formas_pagamento = Input::get('formas_pagamento');
		$object->docentes = Input::get('docentes');
		$object->pre_requisitos = Input::get('pre_requisitos');
		$object->documentacao = Input::get('documentacao');
		$object->observacoes = Input::get('observacoes');

		
		if(sizeof(Cursos::where('slug', '=', $object->slug)->where('id', '!=', $object->id)->get()))
			return Redirect::back()->withErrors(array('Este título já está sendo utilizado!'));	
		
		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Curso alterado com sucesso.');
			return Redirect::route('painel.cursos.index', array('cursos_areas_id' => $object->cursos_areas_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Curso!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Cursos::find($id);
		$area_id = $object->cursos_areas_id;
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Curso removido com sucesso.');

		return Redirect::route('painel.cursos.index', array('cursos_areas_id' => $area_id));
	}

}