<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \ClippingImagens, \Clipping;

class ClippingImagensController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$id = Input::get('clipping_id');

		$clipping = Clipping::find($id);

		if(!$clipping)
			return Redirect::route('painel.clippings.index');

		$this->layout->content = View::make('backend.clippingimagens.index')->with('clipping', $clipping);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.clippingimagens.form')->with('clipping_id', Input::get('clipping_id'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new ClippingImagens;

		$t=Date('YmdHis');
		$imagem = Thumb::make('imagem', 900, null, 'clippingimagens/', $t);
		Thumb::make('imagem', 60, 60, 'clippingimagens/thumbs/', $t);
		if($imagem) $object->imagem = $imagem;
		$object->clipping_id = Input::get('clipping_id');

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Imagem criado com sucesso.');
			return Redirect::route('painel.clippingimagens.index', array('clipping_id' => $object->clipping_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Imagem!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.clippingimagens.edit')->with('registro', ClippingImagens::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = ClippingImagens::find($id);

		$t=Date('YmdHis');
		$imagem = Thumb::make('imagem', 900, null, 'clippingimagens/', $t);
		if($imagem) $object->imagem = $imagem;
		$object->clipping_id = Input::get('clipping_id');

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Imagem alterado com sucesso.');
			return Redirect::route('painel.clippingimagens.index', array('clipping_id' => $object->clipping_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Imagem!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = ClippingImagens::find($id);
		$clipping_id = $object->clipping_id;
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Imagem removido com sucesso.');

		return Redirect::route('painel.clippingimagens.index', array('clipping_id' => $clipping_id));
	}

}