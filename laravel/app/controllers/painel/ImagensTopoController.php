<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, ImagensTopo;

class ImagensTopoController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.imagenstopo.index')->with('registros', ImagensTopo::all());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.imagenstopo.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new ImagensTopo;

		$object->slug = Input::get('slug');
		$imagem = Thumb::make('imagem', 850, 160, 'imagenstopo/');
		if($imagem) $object->imagem = $imagem;

		if(sizeof(ImagensTopo::where('slug', '=', $object->slug)->get()))
			return Redirect::back()->withErrors(array('Essa Área já contém uma imagem!'));	

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Imagem criado com sucesso.');
			return Redirect::route('painel.imagenstopo.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Imagem!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.imagenstopo.edit')->with('registro', ImagensTopo::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = ImagensTopo::find($id);

		$imagem = Thumb::make('imagem', 850, 160, 'imagenstopo/');
		if($imagem) $object->imagem = $imagem;

		if(sizeof(ImagensTopo::where('slug', '=', $object->slug)->where('id', '!=', $object->id)->get()))
			return Redirect::back()->withErrors(array('Essa Área já contém uma imagem!'));

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Imagem alterado com sucesso.');
			return Redirect::route('painel.imagenstopo.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Imagem!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = ImagensTopo::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Imagem removido com sucesso.');

		return Redirect::route('painel.imagenstopo.index');
	}

}