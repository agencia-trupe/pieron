<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, Historia;

class HistoriaController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.historia.index')->with('registros', Historia::orderBy('ordem', 'ASC')->get());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.historia.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Historia;

		$object->titulo = Input::get('titulo');
		$object->titulo_us = Input::get('titulo_us');
		$object->texto = Input::get('texto');
		$object->texto_us = Input::get('texto_us');

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Histórico criado com sucesso.');
			return Redirect::route('painel.historia.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Histórico!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.historia.edit')->with('registro', Historia::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Historia::find($id);

		$object->titulo = Input::get('titulo');
		$object->titulo_us = Input::get('titulo_us');
		$object->texto = Input::get('texto');
		$object->texto_us = Input::get('texto_us');

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Histórico alterado com sucesso.');
			return Redirect::route('painel.historia.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Histórico!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Historia::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Histórico removido com sucesso.');

		return Redirect::route('painel.historia.index');
	}

}