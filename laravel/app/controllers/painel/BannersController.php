<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, banner;

class BannersController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.banners.index')->with('registros', banner::orderBy('ordem', 'ASC')->get());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.banners.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new banner;

		$object->texto = Input::get('texto');
		$object->texto_us = Input::get('texto_us');
		$imagem = Thumb::make('imagem', null, null, 'banners/');
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Banner criado com sucesso.');
			return Redirect::route('painel.banners.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Banner!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.banners.edit')->with('registro', banner::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = banner::find($id);

		$object->texto = Input::get('texto');
		$object->texto_us = Input::get('texto_us');
		$imagem = Thumb::make('imagem', null, null, 'banners/');
		if($imagem) $object->imagem = $imagem;
		
		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Banner alterado com sucesso.');
			return Redirect::route('painel.banners.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao alterar Banner!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = banner::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Banner removido com sucesso.');

		return Redirect::route('painel.banners.index');
	}

}