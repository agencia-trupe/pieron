<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, Unidade;

class UnidadesController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.unidades.index')->with('registros', Unidade::orderBy('ordem', 'ASC')->get());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.unidades.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Unidade;

		$object->cidade = Input::get('cidade');
		$object->estado = Input::get('estado');
		$object->pais = Input::get('pais');
		$object->telefone = Input::get('telefone');
		$object->email = Input::get('email');
		$object->endereco = Input::get('endereco');
		$object->bairro = Input::get('bairro');
		$object->cep = Input::get('cep');
		$object->google_maps = Input::get('google_maps');

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Unidade criado com sucesso.');
			return Redirect::route('painel.unidades.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Unidade!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.unidades.edit')->with('registro', Unidade::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Unidade::find($id);

		//$object->cidade = Input::get('cidade');
		//$object->estado = Input::get('estado');
		//$object->pais = Input::get('pais');
		$object->telefone = Input::get('telefone');
		$object->email = Input::get('email');
		$object->endereco = Input::get('endereco');
		$object->bairro = Input::get('bairro');
		$object->cep = Input::get('cep');
		$object->google_maps = Input::get('google_maps');

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Unidade alterado com sucesso.');
			return Redirect::route('painel.unidades.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Unidade!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Unidade::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Unidade removido com sucesso.');

		return Redirect::route('painel.unidades.index');
	}

}