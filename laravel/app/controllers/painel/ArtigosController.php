<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \Artigo;

class ArtigosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.artigos.index')->with('registros', Artigo::ordenado()->paginate(25));		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.artigos.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Artigo;

		$object->titulo = Input::get('titulo');
		$object->titulo_us = Input::get('titulo_us');
		$object->autor = Input::get('autor');
		$object->data = Tools::converteData(Input::get('data'));
		$object->olho = Input::get('olho');
		$object->olho_us = Input::get('olho_us');
		$object->texto = Input::get('texto');
		$object->texto_us = Input::get('texto_us');

		if(Input::hasFile('pdf')){
			$arquivo = Input::file('pdf');
			$arquivo->move('assets/files/artigos', $arquivo->getClientOriginalName());
			$object->pdf = $arquivo->getClientOriginalName();
		}

		if(Input::has('remover_arquivo') && Input::get('remover_arquivo') == 1)
			$object->pdf = null;
		
		try {

			$object->save();

			$object->slug = \Str::slug(\Tools::slugData(Input::get('data')).' '.Input::get('titulo').' '.$object->id);
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Artigo criado com sucesso.');
			return Redirect::route('painel.artigos.index');

		} catch (\Exception $e) {

			return Redirect::back()->withErrors($e->getMessage());

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.artigos.edit')->with('registro', Artigo::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Artigo::find($id);

		$object->titulo = Input::get('titulo');
		$object->titulo_us = Input::get('titulo_us');
		$object->autor = Input::get('autor');
		$object->data = Tools::converteData(Input::get('data'));
		$object->olho = Input::get('olho');
		$object->olho_us = Input::get('olho_us');
		$object->texto = Input::get('texto');
		$object->texto_us = Input::get('texto_us');
		$object->slug = \Str::slug(\Tools::slugData(Input::get('data')).' '.Input::get('titulo').' '.$object->id);			

		if(Input::hasFile('pdf')){
			$arquivo = Input::file('pdf');
			$arquivo->move('assets/files/artigos', $arquivo->getClientOriginalName());
			$object->pdf = $arquivo->getClientOriginalName();
		}

		if(Input::has('remover_arquivo') && Input::get('remover_arquivo') == 1)
			$object->pdf = null;
		

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Artigo alterado com sucesso.');
			return Redirect::route('painel.artigos.index');

		} catch (\Exception $e) {

			return Redirect::back()->withErrors($e->getMessage());

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Artigo::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Artigo removido com sucesso.');

		return Redirect::route('painel.artigos.index');
	}

}