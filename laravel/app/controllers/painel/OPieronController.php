<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, OPieron;

class OPieronController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.opieron.index')->with('registros', OPieron::orderBy('ordem', 'ASC')->get());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.opieron.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new OPieron;

		$object->titulo = Input::get('titulo');
		$object->titulo_us = Input::get('titulo_us');
		$object->texto = Input::get('texto');
		$object->texto_us = Input::get('texto_us');
		$object->slug = Str::slug(Input::get('titulo'));

		if(sizeof(OPieron::where('slug', '=', $object->slug)->get()))
			return Redirect::back()->withErrors(array('Este título já está sendo utilizado!'));

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto criado com sucesso.');
			return Redirect::route('painel.opieron.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Texto!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.opieron.edit')->with('registro', OPieron::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = OPieron::find($id);

		$object->titulo = Input::get('titulo');
		$object->titulo_us = Input::get('titulo_us');
		$object->texto = Input::get('texto');
		$object->texto_us = Input::get('texto_us');
		if($object->slug != 'proposito' && $object->slug != 'como-trabalhamos' && $object->slug != 'historia' && $object->slug != 'equipe' && $object->slug != 'internacional'){
			$object->slug = Str::slug(Input::get('titulo'));
		}

		if(sizeof(OPieron::where('slug', '=', $object->slug)->where('id', '!=', $object->id)->get()))
			return Redirect::back()->withErrors(array('Este título já está sendo utilizado!'));

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto alterado com sucesso.');
			return Redirect::route('painel.opieron.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Texto!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = OPieron::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Texto removido com sucesso.');

		return Redirect::route('painel.opieron.index');
	}

}