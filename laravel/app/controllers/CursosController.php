<?php

use \Cursos, \CursosAreas, \Unidade, \Turmas, \Str;

class CursosController extends BaseController {

	protected $layout = 'frontend.templates.index';


	public function index($slug_area = false)
	{
		if($slug_area){
			$listaAreas = CursosAreas::where('slug', '=', $slug_area)->get();
		}else{
			$listaAreas = CursosAreas::ordenado();
		}

		if(!$listaAreas) App::abort('404');

		if(Session::has('ordenacaoCursos') && Session::get('ordenacaoCursos') == 'alfabetica')
			$ord = 'alfabetica';
		else
			$ord = 'data_inicio';

		if(Route::currentRouteName() == 'cursos.areas')
			$this->layout->with('css', 'css/cursos');

		$this->layout->content = View::make('frontend.cursos.index')->with('areas', $listaAreas)
																	->with('menuAreas', CursosAreas::ordenado())
																	->with('ordenacao', $ord)
																	->with('slug_area', $slug_area)
																	->nest('formulario', 'frontend.partials.formulario', array('solicitarInfo' => true))
																	->nest('topo', 'frontend.partials.topo', array(
																		'bannerTopo' => ImagensTopo::where('slug', '=', 'cursos')->first(),
																		'textoArea' => Lang::get('base.secoes.cursos')
																	));
	}


	public function detalhes($slug_curso = false)
	{
		if(!$slug_curso) App::abort('404');

		$curso = Cursos::where('slug', '=', $slug_curso)->first();

		if(is_null($curso)) App::abort('404');

		$this->layout->with('css', 'css/cursos');
		$this->layout->content = View::make('frontend.cursos.detalhes')->with('curso', $curso)
																		->with('menuAreas', CursosAreas::ordenado())
																		->with('slug_area', $curso->area->slug)
																		->nest('formulario', 'frontend.partials.formulario', array('solicitarInfo' => true))
																		->nest('topo', 'frontend.partials.topo', array(
																			'bannerTopo' => ImagensTopo::where('slug', '=', 'cursos')->first(),
																			'textoArea' => Lang::get('base.secoes.cursos')
																		));	
	}
}
