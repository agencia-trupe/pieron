<?php

use \Materiais;

class MateriaisController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.materiais.index')->with('detalhe', Materiais::first())
																	   ->nest('formulario', 'frontend.partials.formulario')
																		->nest('topo', 'frontend.partials.topo', array(
																			'bannerTopo' => ImagensTopo::where('slug', '=', 'materiais')->first(),
																			'textoArea' => Lang::get('base.secoes.materiais')
																		));
	}

}
