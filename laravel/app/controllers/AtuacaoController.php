<?php

use \AreasAtuacao, \Atuacao;

class AtuacaoController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index($slug_texto = false)
	{
		if(!$slug_texto)
			$detalhe = Atuacao::first();
		else{
			$detalhe = AreasAtuacao::where('slug', '=', $slug_texto)->first();

			if(is_null($detalhe)){
				App::abort('404');
			}
		}

		$this->layout->with('marcarSub', $detalhe->{Lang::get('base.campos.slug')});
		$this->layout->content = View::make('frontend.atuacao.index')->with('textos', AreasAtuacao::ordenado())
																	 ->with('detalhe', $detalhe)
																	 ->with('slugFiltro', $detalhe->{Lang::get('base.campos.slug')})
																	 ->nest('formulario', 'frontend.partials.formulario')
																	 ->nest('topo', 'frontend.partials.topo', array(
																	 		'bannerTopo' => ImagensTopo::where('slug', '=', 'atuacao')->first(),
																	 		'textoArea' => Lang::get('base.secoes.atuacao')
																	 ));
	}

}
