<?php

use \Unidade;

class ContatoController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.contato.index')->with('unidades', Unidade::ordenado())
																	 ->with('assuntos', AreasAtuacao::ordenado())
																	 ->nest('formulario', 'frontend.partials.formulario', array('camposExtras' => true))
																	 ->nest('topo', 'frontend.partials.topo', array(
																 		'bannerTopo' => ImagensTopo::where('slug', '=', 'contato')->first(),
																 		'textoArea' => Lang::get('base.secoes.contato')
																	 ));
	}

}
