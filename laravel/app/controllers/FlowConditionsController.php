<?php

use \FlowConditions;

class FlowConditionsController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.flowconditions.index')->with('detalhe', FlowConditions::first())
																			->nest('formulario', 'frontend.partials.formulario')
																			->nest('topo', 'frontend.partials.topo', array(
																				'bannerTopo' => ImagensTopo::where('slug', '=', 'flowconditionssystem')->first(),
																				'textoArea' => Lang::get('base.secoes.flowconditions')
																			));
	}

}
