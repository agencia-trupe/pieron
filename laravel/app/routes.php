<?php

Route::get('/', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::post('cadastroNewsletter', 'HomeController@cadastro');
Route::post('envioContato', 'HomeController@contato');
Route::get('home', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('o-pieron/{slug_texto?}', array('as' => 'opieron', 'uses' => 'OPieronController@index'));
Route::get('atuacao/{slug_texto?}', array('as' => 'atuacao', 'uses' => 'AtuacaoController@index'));
Route::get('flow-conditions-system', array('as' => 'flowconditions', 'uses' => 'FlowConditionsController@index'));
Route::get('venda-de-materiais', array('as' => 'materiais', 'uses' => 'MateriaisController@index'));
Route::get('contato', array('as' => 'contato', 'uses' => 'ContatoController@index'));
Route::get('cursos-e-treinamentos', array('as' => 'cursos', 'uses' => 'CursosController@index'));
Route::get('cursos-e-treinamentos/area/{slug_area?}', array('as' => 'cursos.areas', 'uses' => 'CursosController@index'));
Route::get('cursos-e-treinamentos/detalhes/{slug_curso?}', array('as' => 'cursos.detalhes', 'uses' => 'CursosController@detalhes'));
Route::get('publicacoes', array('as' => 'publicacoes', 'uses' => 'PublicacoesController@artigos'));
Route::get('publicacoes/clipping', array('as' => 'publicacoes.clipping', 'uses' => 'PublicacoesController@clipping'));
Route::get('publicacoes/clipping/{slug?}', array('as' => 'publicacoes.clipping.detalhes', 'uses' => 'PublicacoesController@galeria'));
Route::get('publicacoes/artigos', array('as' => 'publicacoes.artigos', 'uses' => 'PublicacoesController@artigos'));
Route::get('publicacoes/artigos/{slug?}', array('as' => 'publicacoes.artigos.detalhes', 'uses' => 'PublicacoesController@artigosDetalhes'));
Route::get('publicacoes/download/{slug?}', array('as' => 'publicacoes.artigos.download', 'uses' => 'PublicacoesController@download'));

Route::get('lang/{sigla_idioma?}', function($sigla_idioma){
	if($sigla_idioma == 'pt' || $sigla_idioma == 'en'){
		Session::put('locale', $sigla_idioma);
		App::setLocale($sigla_idioma);
	}

	return Redirect::back();	
});

Route::get('cursos/ordenarData', function(){
	Session::put('ordenacaoCursos', 'data_inicio');
	return Redirect::back();
});

Route::get('cursos/ordenarNome', function(){
	Session::put('ordenacaoCursos', 'alfabetica');
	return Redirect::back();
});

// Route::any('painel{all}', function(){
// 	return View::make('backend.templates.manutencao');
// })->where('all', '.*');

Route::get('painel', array('before' => 'auth', 'as' => 'painel.home', 'uses' => 'Painel\HomeController@index'));
Route::get('painel/login', array('as' => 'painel.login', 'uses' => 'Painel\HomeController@login'));

//Autenticação do Login
Route::post('painel/login',  array('as' => 'painel.auth', function(){
	$authvars = array(
		'username' => Input::get('username'),
		'password' => Input::get('password')
	);

	$lembrar = (Input::has('lembrar') && Input::get('lembrar') == '1') ? true : false;

	if(Auth::attempt($authvars, $lembrar)){
		return Redirect::to('painel');
	}else{
		Session::flash('login_errors', true);
		return Redirect::to('painel/login');
	}
}));

Route::get('painel/logout', array('as' => 'painel.off', function(){
	Auth::logout();
	return Redirect::to('painel');
}));


Route::post('ajax/gravaOrdem', array('before' => 'auth', 'uses' => 'Painel\AjaxController@gravaOrdem'));

Route::group(array('prefix' => 'painel', 'before' => 'auth'), function()
{

    Route::get('download', function(){

    	$filename = 'cadastros_'.date('d-m-Y-H-i-s');

		Excel::create($filename, function($excel) {
		    $excel->sheet('Usuários Cadastrados', function($sheet) {
		        $sheet->fromModel(\Cadastro::select('nome', 'email')->orderBy('nome')->get());
		    });
		})->download('csv');
    });

    Route::resource('usuarios', 'Painel\UsuariosController');
    Route::resource('banners', 'Painel\BannersController');
	Route::resource('chamadas', 'Painel\ChamadasController');
	Route::resource('cadastros', 'Painel\CadastrosController');
	Route::resource('opieron', 'Painel\OPieronController');
	Route::resource('equipe', 'Painel\EquipeController');
	Route::resource('unidades', 'Painel\UnidadesController');
	Route::resource('cursosareas', 'Painel\CursosAreasController');
	Route::resource('cursos', 'Painel\CursosController');
	Route::resource('turmas', 'Painel\TurmasController');
	Route::resource('materiais', 'Painel\MateriaisController');
	Route::resource('flowconditionssystem', 'Painel\FlowConditionsSystemController');
	Route::resource('atuacao', 'Painel\AtuacaoController');
	Route::resource('areasatuacao', 'Painel\AreasAtuacaoController');
	Route::resource('imagenstopo', 'Painel\ImagensTopoController');
	Route::resource('historia', 'Painel\HistoriaController');
	Route::resource('clippings', 'Painel\ClippingsController');
	Route::resource('artigos', 'Painel\ArtigosController');
	Route::resource('clippingimagens', 'Painel\ClippingImagensController');
	Route::resource('contatos', 'Painel\ContatosController');
//NOVASROTASDOPAINEL//
});