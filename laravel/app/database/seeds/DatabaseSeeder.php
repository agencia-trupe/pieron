<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('ImagensTopoSeeder');
		//$this->call('AtuacaoSeeder');
		//$this->call('FlowConditionsSystemSeeder');
		//$this->call('UsersSeeder');
		//$this->call('MateriaisSeeder');
	}

}
