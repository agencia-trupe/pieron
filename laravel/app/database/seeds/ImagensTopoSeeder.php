<?php

class ImagensTopoSeeder extends Seeder {

    public function run()
    {
        $data = array(
            array(
            	'slug' => 'opieron',
				'imagem' => ''
            ),
            array(
            	'slug' => 'atuacao',
				'imagem' => ''
            ),
            array(
            	'slug' => 'flowconditionssystem',
				'imagem' => ''
            ),
            array(
            	'slug' => 'cursos',
				'imagem' => ''
            ),
            array(
                'slug' => 'contato',
                'imagem' => ''
            ),
            array(
                'slug' => 'materiais',
                'imagem' => ''
            ),
        );

        DB::table('imagens_topo')->insert($data);
    }

}
