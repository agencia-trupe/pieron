<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClippingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clippings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('imagem');
			$table->string('titulo');
			$table->string('titulo_us');
			$table->string('link');
			$table->string('pdf');
			$table->date('data');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clippings');
	}

}
