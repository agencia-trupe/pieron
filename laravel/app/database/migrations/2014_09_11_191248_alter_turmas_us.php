<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTurmasUs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('curso_unidade', function(Blueprint $table)
		{
			$table->text('cronograma_us')->after('cronograma');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('curso_unidade', function(Blueprint $table)
		{
			$table->dropColumn('cronograma_us');
		});
	}

}
