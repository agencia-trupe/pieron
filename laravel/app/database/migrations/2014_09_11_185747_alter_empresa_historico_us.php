<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEmpresaHistoricoUs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('empresa_historico', function(Blueprint $table)
		{
			$table->string('titulo_us')->after('titulo');
			$table->text('texto_us')->after('texto');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('empresa_historico', function(Blueprint $table)
		{
			$table->dropColumn('titulo_us');
			$table->dropColumn('texto_us');
		});
	}

}
