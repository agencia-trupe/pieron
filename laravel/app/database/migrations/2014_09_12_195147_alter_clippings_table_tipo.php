<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterClippingsTableTipo extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('clippings', function(Blueprint $table)
		{
			$table->string('tipo')->after('titulo_us');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('clippings', function(Blueprint $table)
		{
			$table->dropColumn('tipo');
		});
	}

}
