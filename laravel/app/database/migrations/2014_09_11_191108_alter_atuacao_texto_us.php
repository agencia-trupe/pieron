<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAtuacaoTextoUs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('atuacao_texto', function(Blueprint $table)
		{
			$table->text('texto_us')->after('texto');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('atuacao_texto', function(Blueprint $table)
		{
			$table->dropColumn('texto_us');
		});
	}

}
