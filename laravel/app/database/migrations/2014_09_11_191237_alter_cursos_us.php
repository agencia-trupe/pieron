<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCursosUs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cursos', function(Blueprint $table)
		{
			$table->string('titulo_us')->after('titulo');
			$table->text('objetivos_us')->after('objetivos');
			$table->text('publico_alvo_us')->after('publico_alvo');
			$table->text('metodologia_us')->after('metodologia');
			$table->text('conteudo_us')->after('conteudo');
			$table->text('carga_horaria_us')->after('carga_horaria');
			$table->text('valor_us')->after('valor');
			$table->text('formas_pagamento_us')->after('formas_pagamento');
			$table->text('docentes_us')->after('docentes');
			$table->text('pre_requisitos_us')->after('pre_requisitos');
			$table->text('documentacao_us')->after('documentacao');
			$table->text('observacoes_us')->after('observacoes');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cursos', function(Blueprint $table)
		{
			$table->dropColumn('titulo_us');
			$table->dropColumn('objetivos_us');
			$table->dropColumn('publico_alvo_us');
			$table->dropColumn('metodologia_us');
			$table->dropColumn('conteudo_us');
			$table->dropColumn('carga_horaria_us');
			$table->dropColumn('valor_us');
			$table->dropColumn('formas_pagamento_us');
			$table->dropColumn('docentes_us');
			$table->dropColumn('pre_requisitos_us');
			$table->dropColumn('documentacao_us');
			$table->dropColumn('observacoes_us');
		});
	}

}
