<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEnmpresaEquipeUs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('empresa_equipe', function(Blueprint $table)
		{
			$table->text('olho_us')->after('olho');
			$table->text('texto_us')->after('texto');
			$table->string('cargo_us')->after('cargo');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('empresa_equipe', function(Blueprint $table)
		{
			$table->dropColumn('olho_us');
			$table->dropColumn('texto_us');
			$table->dropColumn('cargo_us');
		});
	}

}
