<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCursosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cursos', function(Blueprint $table)
		{
			$table->text('pre_requisitos')->after('docentes');
			$table->text('documentacao')->after('pre_requisitos');
			$table->text('observacoes')->after('documentacao');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cursos', function(Blueprint $table)
		{
			$table->dropColumn('pre_requisitos');
			$table->dropColumn('documentacao');
			$table->dropColumn('observacoes');
		});
	}

}
