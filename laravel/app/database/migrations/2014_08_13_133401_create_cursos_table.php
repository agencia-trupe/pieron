<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCursosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cursos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('cursos_areas_id');
			$table->string('titulo');
			$table->string('slug');
			$table->text('objetivos');
			$table->text('publico_alvo');
			$table->text('metodologia');
			$table->text('conteudo');
			$table->text('carga_horaria');
			$table->text('valor');
			$table->text('formas_pagamento');
			$table->text('docentes');
			$table->integer('ordem');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cursos');
	}

}
