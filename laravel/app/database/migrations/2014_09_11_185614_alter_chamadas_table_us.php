<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterChamadasTableUs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('chamadas', function(Blueprint $table)
		{
			$table->string('titulo_us')->after('titulo');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('chamadas', function(Blueprint $table)
		{
			$table->dropColumn('titulo_us');
		});
	}

}
