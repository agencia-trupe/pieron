<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaEquipeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('empresa_equipe', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nome');
			$table->text('olho');
			$table->text('texto');
			$table->string('imagem');
			$table->string('cargo');
			$table->integer('ordem');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('empresa_equipe');
	}

}
