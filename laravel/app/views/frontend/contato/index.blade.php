@section('conteudo')
	
	<div class="centro">
		
		{{$topo}}

		<div id="miolo" class="pure-g">
			
			{{$formulario}}			

			<section class="pure-u-3-4 pure-u-tablet-1 pure-u-mobile-1">

				<div class="pad">

					<h1>{{Lang::get('base.secoes.contato')}}</h1>

					@if($unidades)
						<div class="listaUnidades">
							@foreach($unidades as $unidade)
								<div class="unidade">
									<div class="info">
										<h2>{{$unidade->cidade.' &middot; '.$unidade->telefone}}</h2>
										<div class="endereco">
											<strong>{{Lang::get('base.geral.institutoPieron').' | '.$unidade->cidade}}</strong> | <a href="mailto:{{$unidade->email}}" title="E-mail">{{$unidade->email}}</a><br>
											{{$unidade->endereco.' &middot; '.$unidade->bairro.' &middot; '.$unidade->cep.' &middot; '.$unidade->cidade.'/'.$unidade->estado.' &middot; '.$unidade->pais}}
										</div>
									</div>
									<div class="mapa">
										{{Tools::viewGMaps($unidade->google_maps, '100%', '250px')}}
									</div>
								</div>
							@endforeach
						</div>
					@endif

				</div>
				
			</section>

			

		</div>

	</div>

@stop
