@section('conteudo')

	<div class="centro">
		
		{{$topo}}

		<div id="miolo" class="pure-g">
			
			<aside class="pure-u-1-4 pure-u-tablet-1 pure-u-mobile-1">
				
				<h1>{{Lang::get('base.secoes.materiais')}}</h1>
				
			</aside>

			<section class="pure-u-1-2 pure-u-tablet-1">

				@if($detalhe)

					<h1>{{Lang::get('base.secoes.materiais')}}</h1>
					
					<div class="cke">{{$detalhe->{Lang::get('base.campos.texto')} }}</div>
					
				@endif
				
			</section>

			{{$formulario}}

		</div>

	</div>

@stop