<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Instituto Pieron" />
    <meta name="copyright" content="1997-2014 Instituto Pieron" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    
    <meta name="keywords" content="pieron, consultoria, assessment, desenvolvimento, coaching, flow, cursos, Insights, flow, Flow Conditions System, Marcos Luiz Bruno, Elisabeth Beran Bruno. Rafael Beran Bruno" />

	<title>Pieron</title>
	<meta name="description" content="Acreditamos que pessoas e organizações podem ir além e superar expectativas. Trabalhamos para enxergar e criar valor nas interação entre as organizações e suas pessoas e fazemos isso com ações que direcionam as organizações ao flow e geram resultados visíveis: rentabilidade, satisfação dos clientes e pleno engajamento das equipes.">
	<meta property="og:title" content="Pieron"/>
	<meta property="og:description" content="Acreditamos que pessoas e organizações podem ir além e superar expectativas. Trabalhamos para enxergar e criar valor nas interação entre as organizações e suas pessoas e fazemos isso com ações que direcionam as organizações ao flow e geram resultados visíveis: rentabilidade, satisfação dos clientes e pleno engajamento das equipes."/>

    <meta property="og:site_name" content="Pieron"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content="{{ Request::url() }}"/>

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>

	<?=Assets::JS(array('vendor/modernizr/modernizr'))?>

	<?=Assets::CSS(array('vendor/reset-css/reset'))?>

	<!--[if lt IE 9]>
		<link rel="stylesheet" href="assets/vendor/pure/grids-responsive-old-ie-min.css">
		<link rel="stylesheet" href="assets/css/custom-pure/grid-old-ie.css">
		<link rel="stylesheet" href="assets/css/ie8.css">
	<![endif]-->
	<!--[if (gte IE 9) | (!IE)]><!-->  
		<link rel="stylesheet" href="assets/vendor/pure/grids-min.css">
		<link rel="stylesheet" href="assets/css/custom-pure/grid.css">
		<?=Assets::CSS(array(
			'css/fontface/stylesheet',
			'vendor/fancybox/source/jquery.fancybox',
			'css/base',
			'css/'.str_replace('-', '', Route::currentRouteName())
		))?>
	<!--<![endif]-->

	@if(isset($css))
		<?=Assets::CSS(array($css))?>
	@endif
	
	<!--[if lt IE 9]>  
	    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>	    
	<![endif]-->  
	<!--[if (gte IE 9) | (!IE)]><!-->  
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="{{asset('vendor/jquery/dist/jquery.min.js')}}"><\/script>')</script>	    
	
		@if(App::environment()=='local')
			<?=Assets::JS(array('vendor/less.js/dist/less-1.6.2.min'))?>
		@endif
	<!--<![endif]-->



	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-54196603-1', 'auto');
	  ga('send', 'pageview');

	</script>
</head>
<body>

	<header>
		<nav>
			<ul @if(str_is('home', Route::currentRouteName())) class="mobileAberto" @endif>
				<li id="mobileAbrirMenu"><a href="#" title="abrir menu"><img src="assets/images/layout/menu-celular.png" alt="abrir menu"></a></li>
				<li><a href="o-pieron" class="comsub sub-pieron @if(str_is('opieron*', Route::currentRouteName())) ativo @endif" title="{{Lang::get('base.menu.opieron')}}">{{Lang::get('base.menu.opieron')}}</a></li>
				<li><a href="atuacao" class="comsub sub-atuacao @if(str_is('atuacao*', Route::currentRouteName())) ativo @endif" title="{{Lang::get('base.menu.atuacao')}}">{{Lang::get('base.menu.atuacao')}}</a></li>
				<li><a href="flow-conditions-system" @if(str_is('flow*', Route::currentRouteName())) class="ativo" @endif title="{{Lang::get('base.menu.flowcondition')}}">{{Lang::get('base.menu.flowcondition')}}</a></li>
				<li><a href="publicacoes" @if(str_is('publicacoes*', Route::currentRouteName())) class="ativo" @endif title="{{Lang::get('base.menu.publicacoes')}}">{{Lang::get('base.menu.publicacoes')}}</a></li>
				<li><a href="cursos-e-treinamentos" @if(str_is('cursos*', Route::currentRouteName())) class="ativo" @endif title="{{Lang::get('base.menu.cursos')}}">{{Lang::get('base.menu.cursos')}}</a></li>
				<li><a href="contato" @if(str_is('contato*', Route::currentRouteName())) class="ativo" @endif title="{{Lang::get('base.menu.contato')}}">{{Lang::get('base.menu.contato')}}</a></li>
				<li id="linkLang"><a href="lang/{{Lang::get('base.menu.abbr')}}" title="{{Lang::get('base.menu.versao')}}">[{{Lang::get('base.menu.lang')}}]</a></li>
			</ul>
		</nav>
		<div class="submenu">
			<div class="centro">
				<ul class="sub-pieron">
					@if(sizeof($subOPieron))
						@foreach($subOPieron as $sub)
							<li><a href="o-pieron/{{$sub->slug}}" title="{{$sub->{Lang::get('base.campos.titulo')} }}" @if(isset($marcarSub) && ($marcarSub == $sub->slug)) class="ativo" @endif>{{$sub->{Lang::get('base.campos.titulo')} }}</a></li>
						@endforeach
					@endif
				</ul>
				<ul class="sub-atuacao">
					@if(sizeof($subAtuacao))
						@foreach($subAtuacao as $sub)
							<li><a href="atuacao/{{$sub->slug}}" title="{{$sub->{Lang::get('base.campos.titulo')} }}" @if(isset($marcarSub) && ($marcarSub == $sub->slug)) class="ativo" @endif>{{$sub->{Lang::get('base.campos.titulo')} }}</a></li>
						@endforeach
					@endif
				</ul>
			</div>
		</div>
	</header>

	<!-- Conteúdo Principal -->
	<div class="main {{ 'main-'. str_replace('-', '', Route::currentRouteName()) }}">
		@yield('conteudo')
	</div>

	<footer>
		<div class="centro">

			<a href="#" id="scrollTopBtn" title="Voltar ao topo">TOPO</a>

			<ul>
				<li class="toplink"><a href="o-pieron" title="{{Lang::get('base.menu.opieron')}}">{{Lang::get('base.menu.opieron')}}</a></li>
				@if(sizeof($subOPieron))
					@foreach($subOPieron as $sub)
						<li><a href="o-pieron/{{$sub->slug}}" title="{{$sub->{Lang::get('base.campos.titulo')} }}">{{$sub->{Lang::get('base.campos.titulo')} }}</a></li>
					@endforeach
				@endif
			</ul>

			<ul>
				<li class="toplink"><a href="atuacao" title="{{Lang::get('base.menu.atuacao')}}">{{Lang::get('base.menu.atuacao')}}</a></li>
				@if(sizeof($subAtuacao))
					@foreach($subAtuacao as $sub)
						<li><a href="atuacao/{{$sub->slug}}" title="{{$sub->{Lang::get('base.campos.titulo')} }}">{{$sub->{Lang::get('base.campos.titulo')} }}</a></li>
					@endforeach
				@endif
			</ul>

			<ul>
				<li class="toplink"><a href="flow-conditions-system" title="{{Lang::get('base.menu.flowcondition')}}">{{Lang::get('base.menu.flowcondition')}}</a></li>
				<li class="toplink"><a href="publicacoes" title="{{Lang::get('base.menu.publicacoes')}}">{{Lang::get('base.menu.publicacoes')}}</a></li>
				<li class="toplink"><a href="cursos-e-treinamentos" title="{{Lang::get('base.menu.cursos')}}">{{Lang::get('base.menu.cursos')}}</a></li>
				<li class="toplink"><a href="contato" title="{{Lang::get('base.menu.contato')}}">{{Lang::get('base.menu.contato')}}</a></li>
				<li class="toplink"><a href="venda-de-materiais" title="{{Lang::get('base.menu.materiais')}}">{{Lang::get('base.menu.materiais')}}</a></li>
			</ul>

			<div class="listaUnidades">
				@if(sizeof($listaUnidades))
					@foreach($listaUnidades as $sub)
						<div class="unidade">
							<h2>{{Lang::get('base.geral.institutoPieron')}} - {{$sub->cidade}}</h2>
							<p>
								{{$sub->telefone}}<br>
								<a href="mailto:{{$sub->email}}" title="E-mail">{{$sub->email}}</a><br>
								{{$sub->endereco.' - '.$sub->bairro}}<br>
								{{$sub->cep.' - '.$sub->cidade.'/'.$sub->estado}}<br>
							</p>
						</div>
					@endforeach
				@endif
			</div>

			<div class="marcaPieron">
				<img src="assets/images/layout/marca-pieron-rodape.png" alt="{{Lang::get('base.geral.institutoPieron')}}">
				<span>{{Lang::get('base.geral.biossInternational')}}</span>
			</div>

		</div>
		<div class="assinatura">
			{{Lang::get('base.geral.assinatura')}}
		</div>
	
	</footer>

	<?=Assets::JS(array(
		'vendor/fancybox/source/jquery.fancybox',
		'vendor/jquery.cycle/jquery.cycle.all',
		'js/customselect',
		'js/main'
	))?>

</body>
</html>
