<html>
	<head>
		<style>
			body{
				border-top:40px #00509F solid;
			}
			h1{
				text-align:center;
				margin:50px auto;
				color:#00509F;
				font-size: 40px;
				clear:left;
			}
			a#logo-link{
				float:left;
				margin:15px 0;
			}
			a#back-link{
				display:block;
				width:200px;
				text-align:center;
				margin:0 auto;
				background:#00509F;
				color:#fff;
				text-decoration: none;
				font-size:15px;
				font-weight: bold;
				padding:10px;
			}
		</style>
	</head>
	<body>
		<a href="http://www.pieron.com.br" id="logo-link">
			<img src="assets/images/layout/marca-pieron.png" alt="Pieron">
		</a>
		<h1>Erro na solicitação.</h1>
		<a href="http://www.pieron.com.br" id="back-link">&larr; voltar ao site Pieron</a>
	</body>
</html>
