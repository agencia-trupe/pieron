@extends('frontend.templates.index')
@section('conteudo')

	<div class="centro">
		<div id="topo" class="pure-g">
			
			<div class="pure-u-1-4">
				<a href="home" id="logo-link" title="{{Lang::get('base.menu.paginaInicial')}}">
					<img src="assets/images/layout/marca-pieron.png" alt="{{Lang::get('base.geral.institutoPieron')}}">
				</a>
			</div>

			<div id="banner-topo" class="pure-u-3-4">
				@if($bannerTopo)
					<img src="assets/images/imagenstopo/{{$bannerTopo->imagem}}" alt="{{$textoArea}}">
				@endif
			</div>
		</div>

		<div style="min-height:300px;">
			<h1 id='pagenotfound'>Página não encontrada</h1>
		</div>
	</div>

@stop
