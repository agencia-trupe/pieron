@section('conteudo')
	
	<div class="centro">
		
		{{$topo}}

		<div id="miolo" class="pure-g">
			
			<aside class="pure-u-1-4 pure-u-tablet-1 pure-u-mobile-1">
				
				<h1>{{Lang::get('base.secoes.cursos')}}</h1>
				
				@if(sizeof($menuAreas))
					<ul>
						@foreach($menuAreas as $area)
							<li><a href="cursos-e-treinamentos/area/{{$area->slug }}" @if($area->slug == $slug_area) class="ativo" @endif title="{{$area->titulo }}">{{$area->titulo }}</a></li>
						@endforeach
					</ul>
				@endif

			</aside>

			<section class="pure-u-1-2 pure-u-tablet-1 pure-u-mobile-1">

				<div class="pad">

					<h1>{{Lang::get('base.secoes.cursos')}}</h1>

					<h2>{{$curso->area->titulo }}</h2>
					<h3>{{$curso->titulo }}</h3>

					@if(sizeof($curso->turmas))
						<h4>{{Lang::get('base.cursos.proximasTurmas')}}</h4>

						@foreach($curso->turmas as $turma)
							<div class="listaTurmas">
								<table class="turma">
									<thead>
										<tr>
											<th>{{Tools::converteData($turma->data_inicio)}}</th>
											<th>{{Lang::get('base.cursos.unidade')}} <span>{{$turma->unidade->cidade}}</span></th>
										</tr>
									</thead>
									<tbody>
										<tr class="topo">
											<td>{{Lang::get('base.cursos.tabelaCronograma')}}</td>
											<td>{{Lang::get('base.cursos.tabelaLocal')}}</td>
										</tr>
										<tr>
											<td>{{nl2br($turma->cronograma)}}</td>
											<td>
												{{Lang::get('base.geral.institutoPieron').' - '.$turma->unidade->cidade .' | '.$turma->unidade->telefone}}<br>
												{{$turma->unidade->endereco.' - '.$turma->unidade->bairro.' <br> '.$turma->unidade->cep.' - '.$turma->unidade->cidade.'/'.$turma->unidade->estado}}
											</td>
										</tr>
									</tbody>
								</table>
								@if($turma->unidade->cidade == 'Campinas')
									<a href="http://pieron.com.br/_cadastro_alunos/cad_alun.php?unin=cp" class="botao-inscricao" title="{{Lang::get('base.cursos.inscreva-se')}}" target="_blank">{{Lang::get('base.cursos.inscreva-se')}}</a>
								@else
									<a href="http://pieron.com.br/_cadastro_alunos/cad_alun.php?unin=sp" class="botao-inscricao" title="{{Lang::get('base.cursos.inscreva-se')}}" target="_blank">{{Lang::get('base.cursos.inscreva-se')}}</a>
								@endif
							</div>
						@endforeach
					@else
						<h4 class="semturma">{{Lang::get('base.cursos.semTurma')}}</h4>
					@endif


					@if($curso->objetivos)
						<div class="conteudo cke">
							<h5>{{Lang::get('base.cursos.labelobjetivos')}}</h5>
							{{$curso->objetivos }}
						</div>
					@endif

					@if($curso->publico_alvo)
						<div class="conteudo cke">
							<h5>{{Lang::get('base.cursos.labelpublico_alvo')}}</h5>
							{{$curso->publico_alvo }}
						</div>
					@endif

					@if($curso->metodologia)
						<div class="conteudo cke">
							<h5>{{Lang::get('base.cursos.labelmetodologia')}}</h5>
							{{$curso->metodologia }}
						</div>
					@endif

					@if($curso->conteudo)
						<div class="conteudo cke">
							<h5>{{Lang::get('base.cursos.labelconteudo')}}</h5>
							{{$curso->conteudo }}
						</div>
					@endif

					@if($curso->carga_horaria)
						<div class="conteudo cke">
							<h5>{{Lang::get('base.cursos.labelcarga_horaria')}}</h5>
							{{$curso->carga_horaria }}
						</div>
					@endif

					@if($curso->valor)
						<div class="conteudo cke">
							<h5>{{Lang::get('base.cursos.labelvalor')}}</h5>
							{{$curso->valor }}
						</div>
					@endif

					@if($curso->formas_pagamento)
						<div class="conteudo cke">
							<h5>{{Lang::get('base.cursos.labelformas_pagamento')}}</h5>
							{{$curso->formas_pagamento }}
						</div>
					@endif

					@if($curso->docentes)
						<div class="conteudo cke">
							<h5>{{Lang::get('base.cursos.labeldocentes')}}</h5>
							{{$curso->docentes }}
						</div>
					@endif

					@if($curso->pre_requisitos)
						<div class="conteudo cke">
							<h5>{{Lang::get('base.cursos.labelpre_requisitos')}}</h5>
							{{$curso->pre_requisitos }}
						</div>
					@endif

					@if($curso->documentacao)
						<div class="conteudo cke">
							<h5>{{Lang::get('base.cursos.labeldocumentacao')}}</h5>
							{{$curso->documentacao }}
						</div>
					@endif

					@if($curso->observacoes)
						<div class="conteudo cke">
							<h5>{{Lang::get('base.cursos.labelobservacoes')}}</h5>
							{{$curso->observacoes }}
						</div>
					@endif

				</div>
				
			</section>

			{{$formulario}}

		</div>

	</div>

@stop
