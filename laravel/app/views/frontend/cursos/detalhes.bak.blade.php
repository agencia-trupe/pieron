@section('conteudo')
	
	<div class="centro">
		
		{{$topo}}

		<div id="miolo" class="pure-g">
			
			<aside class="pure-u-1-4 pure-u-tablet-1 pure-u-mobile-1">
				
				<h1>{{Lang::get('base.secoes.cursos')}}</h1>
				
				@if(sizeof($menuAreas))
					<ul>
						@foreach($menuAreas as $area)
							<li><a href="cursos-e-treinamentos/area/{{$area->{Lang::get('base.campos.slug')} }}" @if($area->{Lang::get('base.campos.slug')} == $slug_area) class="ativo" @endif title="{{$area->{Lang::get('base.campos.titulo')} }}">{{$area->{Lang::get('base.campos.titulo')} }}</a></li>
						@endforeach
					</ul>
				@endif

			</aside>

			<section class="pure-u-1-2 pure-u-tablet-1 pure-u-mobile-1">

				<div class="pad">

					<h1>{{Lang::get('base.secoes.cursos')}}</h1>

					<h2>{{$curso->area->{Lang::get('base.campos.titulo')} }}</h2>
					<h3>{{$curso->{Lang::get('base.campos.titulo')} }}</h3>

					@if(sizeof($curso->turmas))
						<h4>{{Lang::get('base.cursos.proximasTurmas')}}</h4>

						@foreach($curso->turmas as $turma)
							<div class="listaTurmas">
								<table class="turma">
									<thead>
										<tr>
											<th>{{Tools::converteData($turma->data_inicio)}}</th>
											<th>{{Lang::get('base.cursos.unidade')}} <span>{{$turma->unidade->cidade}}</span></th>
										</tr>
									</thead>
									<tbody>
										<tr class="topo">
											<td>{{Lang::get('base.cursos.tabelaCronograma')}}</td>
											<td>{{Lang::get('base.cursos.tabelaLocal')}}</td>
										</tr>
										<tr>
											<td>{{nl2br($turma->{Lang::get('base.campos.cronograma')})}}</td>
											<td>
												{{Lang::get('base.geral.institutoPieron').' - '.$turma->unidade->cidade .' | '.$turma->unidade->telefone}}<br>
												{{$turma->unidade->endereco.' - '.$turma->unidade->bairro.' <br> '.$turma->unidade->cep.' - '.$turma->unidade->cidade.'/'.$turma->unidade->estado}}
											</td>
										</tr>
									</tbody>
								</table>
								@if($turma->unidade->cidade == 'Campinas')
									<a href="http://pieron.com.br/_cadastro_alunos/cad_alun.php?unin=cp" class="botao-inscricao" title="{{Lang::get('base.cursos.inscreva-se')}}" target="_blank">{{Lang::get('base.cursos.inscreva-se')}}</a>
								@else
									<a href="http://pieron.com.br/_cadastro_alunos/cad_alun.php?unin=sp" class="botao-inscricao" title="{{Lang::get('base.cursos.inscreva-se')}}" target="_blank">{{Lang::get('base.cursos.inscreva-se')}}</a>
								@endif
							</div>
						@endforeach
					@else
						<h4 class="semturma">{{Lang::get('base.cursos.semTurma')}}</h4>
					@endif


					@if($curso->{Lang::get('base.campos.objetivos')})
						<div class="conteudo cke">
							<h5>{{Lang::get('base.cursos.labelobjetivos')}}</h5>
							{{$curso->{Lang::get('base.campos.objetivos')} }}
						</div>
					@endif

					@if($curso->{Lang::get('base.campos.publico_alvo')})
						<div class="conteudo cke">
							<h5>{{Lang::get('base.cursos.labelpublico_alvo')}}</h5>
							{{$curso->{Lang::get('base.campos.publico_alvo')} }}
						</div>
					@endif

					@if($curso->{Lang::get('base.campos.metodologia')})
						<div class="conteudo cke">
							<h5>{{Lang::get('base.cursos.labelmetodologia')}}</h5>
							{{$curso->{Lang::get('base.campos.metodologia')} }}
						</div>
					@endif

					@if($curso->{Lang::get('base.campos.conteudo')})
						<div class="conteudo cke">
							<h5>{{Lang::get('base.cursos.labelconteudo')}}</h5>
							{{$curso->{Lang::get('base.campos.conteudo')} }}
						</div>
					@endif

					@if($curso->{Lang::get('base.campos.carga_horaria')})
						<div class="conteudo cke">
							<h5>{{Lang::get('base.cursos.labelcarga_horaria')}}</h5>
							{{$curso->{Lang::get('base.campos.carga_horaria')} }}
						</div>
					@endif

					@if($curso->{Lang::get('base.campos.valor')})
						<div class="conteudo cke">
							<h5>{{Lang::get('base.cursos.labelvalor')}}</h5>
							{{$curso->{Lang::get('base.campos.valor')} }}
						</div>
					@endif

					@if($curso->{Lang::get('base.campos.formas_pagamento')})
						<div class="conteudo cke">
							<h5>{{Lang::get('base.cursos.labelformas_pagamento')}}</h5>
							{{$curso->{Lang::get('base.campos.formas_pagamento')} }}
						</div>
					@endif

					@if($curso->{Lang::get('base.campos.docentes')})
						<div class="conteudo cke">
							<h5>{{Lang::get('base.cursos.labeldocentes')}}</h5>
							{{$curso->{Lang::get('base.campos.docentes')} }}
						</div>
					@endif

					@if($curso->{Lang::get('base.campos.pre_requisitos')})
						<div class="conteudo cke">
							<h5>{{Lang::get('base.cursos.labelpre_requisitos')}}</h5>
							{{$curso->{Lang::get('base.campos.pre_requisitos')} }}
						</div>
					@endif

					@if($curso->{Lang::get('base.campos.documentacao')})
						<div class="conteudo cke">
							<h5>{{Lang::get('base.cursos.labeldocumentacao')}}</h5>
							{{$curso->{Lang::get('base.campos.documentacao')} }}
						</div>
					@endif

					@if($curso->{Lang::get('base.campos.observacoes')})
						<div class="conteudo cke">
							<h5>{{Lang::get('base.cursos.labelobservacoes')}}</h5>
							{{$curso->{Lang::get('base.campos.observacoes')} }}
						</div>
					@endif

				</div>
				
			</section>

			{{$formulario}}

		</div>

	</div>

@stop
