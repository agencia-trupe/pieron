@section('conteudo')
	
	<div class="centro">
		
		{{$topo}}

		<div id="miolo" class="pure-g">
			
			<aside class="pure-u-1-4 pure-u-tablet-1 pure-u-mobile-1">
				
				<h1>{{Lang::get('base.secoes.cursos')}}</h1>
				
				@if(sizeof($menuAreas))
					<ul>
						@foreach($menuAreas as $area)
							<li><a href="cursos-e-treinamentos/area/{{$area->slug }}" @if($area->slug == $slug_area) class="ativo" @endif title="{{$area->titulo }}">{{$area->titulo }}</a></li>
						@endforeach
					</ul>
				@endif

			</aside>

			<section class="pure-u-1-2 pure-u-tablet-1 pure-u-mobile-1">
				
				<div class="pad indexCursos">

					<h1>{{Lang::get('base.secoes.cursos')}}</h1>

					<p class="cursosOlho">{{Lang::get('base.cursos.saibaMais')}}</p>

					<div id="filtro">
						<a href="cursos/ordenarData" title="{{Lang::get('base.cursos.ordenarData')}}" @if($ordenacao == 'data_inicio') class="selecionado" @endif>{{Lang::get('base.cursos.ordenarData')}}</a>
						<a href="cursos/ordenarNome" title="{{Lang::get('base.cursos.ordenarNome')}}" @if($ordenacao == 'alfabetica') class="selecionado" @endif>{{Lang::get('base.cursos.ordenarNome')}}</a>
						<select name="select-filtra-cursos" class="selectOriginalCursos" id="filtra-cursos">
							<option value="">{{Lang::get('base.cursos.selectArea')}}</option>
							@if(sizeof($menuAreas))
								<optgroup>
									@foreach($menuAreas as $area)
										<option value="{{$area->slug }}" @if($area->slug == $slug_area) selected @endif>{{$area->titulo }}</option>
									@endforeach
								</optgroup>
							@endif
						</select>
					</div>

					<div id="lista-cursos">
						<table>
							<thead>
								<tr>
									<th>{{Lang::get('base.cursos.headerCurso')}}</th>
									<th>{{Lang::get('base.cursos.headerData')}}</th>
									<th>{{Lang::get('base.cursos.headerLocal')}}</th>
								</tr>
							</thead>
							<tbody>
								@if($areas)

									@foreach($areas as $area)

										<tr class="titulo">
											<td colspan="3">{{$area->titulo }}</td>
										</tr>

										@if($ordenacao == 'data_inicio')

											@foreach($area->cursosOrdenadoPorData() as $curso)
												@if($curso->cursotitulo != '')
													<tr>
														<td>
															<a href="cursos-e-treinamentos/detalhes/{{$curso->{Lang::get('base.campos.cursoslug')} }}" title="{{$curso->{Lang::get('base.campos.cursotitulo')} }}">{{$curso->{Lang::get('base.campos.cursotitulo')} }}</a>
														</td>

														@if($curso->turmasdata)

															<td>
																<a href="cursos-e-treinamentos/detalhes/{{$curso->{Lang::get('base.campos.cursoslug')} }}" title="{{$curso->{Lang::get('base.campos.cursotitulo')} }}">{{Tools::converteData($curso->turmasdata)}}</a>
															</td>
															<td>
																<a href="cursos-e-treinamentos/detalhes/{{$curso->{Lang::get('base.campos.cursoslug')} }}" title="{{$curso->{Lang::get('base.campos.cursotitulo')} }}">{{Unidade::find($curso->turmaunidade)->cidade}}</a>
															</td>

														@else

															<td>
																<a href="cursos-e-treinamentos/detalhes/{{$curso->{Lang::get('base.campos.cursoslug')} }}" title="{{$curso->{Lang::get('base.campos.cursotitulo')} }}">--</a>
															</td>
															<td>
																<a href="cursos-e-treinamentos/detalhes/{{$curso->{Lang::get('base.campos.cursoslug')} }}" title="{{$curso->{Lang::get('base.campos.cursotitulo')} }}">--</a>
															</td>

														@endif
													</tr>
												@endif
											@endforeach

										@else

											@if(sizeof($area->cursosPorNome))
												@foreach($area->cursosPorNome as $curso)
													@if(sizeof($curso->turmas))
														@foreach($curso->turmas as $turma)

															<tr>
																<td>
																	<a href="cursos-e-treinamentos/detalhes/{{$curso->slug }}" title="{{$curso->titulo }}">{{$curso->titulo }}</a>
																</td>
																<td>
																	<a href="cursos-e-treinamentos/detalhes/{{$curso->slug }}" title="{{$curso->titulo }}">{{Tools::converteData($turma->data_inicio)}}</a>
																</td>
																<td>
																	<a href="cursos-e-treinamentos/detalhes/{{$curso->slug }}" title="{{$curso->titulo }}">{{Unidade::find($turma->unidade_id)->cidade}}</a>
																</td>
															</tr>

														@endforeach

													@else

														<tr>
															<td>
																<a href="cursos-e-treinamentos/detalhes/{{$curso->slug }}" title="{{$curso->titulo }}">{{$curso->titulo }}</a>
															</td>
															<td>
																<a href="cursos-e-treinamentos/detalhes/{{$curso->slug }}" title="{{$curso->titulo }}">--</a>
															</td>
															<td>
																<a href="cursos-e-treinamentos/detalhes/{{$curso->slug }}" title="{{$curso->titulo }}">--</a>
															</td>															
														</tr>

													@endif
												@endforeach
											@endif

										@endif
										
									@endforeach
								@endif
							</tbody>
						</table>
					</div>

				</div>
				
			</section>

			{{$formulario}}

		</div>

	</div>

@stop
