@section('conteudo')

	<div class="centro">
			
		{{$topo}}

		<div id="miolo" class="pure-g">
			
			<aside class="pure-u-1-4 pure-u-tablet-1 pure-u-mobile-1">
				
				<h1>{{Lang::get('base.secoes.opieron')}}</h1>

				@if(sizeof($textos))
					<ul>
						@foreach($textos as $texto)
							<li><a href="o-pieron/{{$texto->{Lang::get('base.campos.slug')} }}" @if($texto->{Lang::get('base.campos.slug')} == $slugFiltro) class="ativo" @endif title="{{$texto->{Lang::get('base.campos.titulo')} }}">{{$texto->{Lang::get('base.campos.titulo')} }}</a></li>
						@endforeach
					</ul>
				@endif
				
			</aside>

			<section class="pure-u-1-2 pure-u-tablet-1 pure-u-mobile-1">

				@if($detalhe)
					<h1>{{$detalhe->{Lang::get('base.campos.titulo')} }}</h1>

					<div class="cke">{{$detalhe->{Lang::get('base.campos.texto')} }}</div>
				@endif

				@if($slugFiltro == 'historia' && $listaHistoria)
					<div id="listaHistoria">
						@foreach($listaHistoria as $item)
							<div class="historico">
								<h2>{{$item->{Lang::get('base.campos.titulo')} }}</h2>
								<div class="cke">{{$item->{Lang::get('base.campos.texto')} }}</div>
							</div>
						@endforeach
					</div>
				@endif

				@if($slugFiltro == 'equipe' && $listaEquipe)
					<div id="listaEquipe">
						@foreach($listaEquipe as $item)
							<div class="membroEquipe">
								<div class="superior">
									<img src="assets/images/equipe/{{$item->imagem}}" alt="{{$item->{Lang::get('base.campos.titulo')} }}">
									<h2>{{$item->{Lang::get('base.campos.nome')} }}</h2>
									<h3>{{$item->{Lang::get('base.campos.cargo')} }}</h3>
									<div class="olho cke">{{$item->{Lang::get('base.campos.olho')} }}</div>
								</div>
								<div class="detalhe cke">{{$item->{Lang::get('base.campos.texto')} }}</div>
								<a href="#" class="botao-toggle" data-abrir="{{Lang::get('base.geral.cvCompleto')}}" data-fechar="{{Lang::get('base.geral.fecharCV')}}" title="{{Lang::get('base.geral.cvCompleto')}}">{{Lang::get('base.geral.cvCompleto')}}</a>
							</div>
						@endforeach
					</div>
				@endif
				
			</section>

			{{$formulario}}

		</div>

	</div>

@stop