@section('conteudo')

	<div class="centro">
		
		{{$topo}}

		<div id="miolo" class="pure-g">
			
			<aside class="pure-u-1-4 pure-u-tablet-1 pure-u-mobile-1">
				
				<h1>{{Lang::get('base.secoes.atuacao')}}</h1>

				@if(sizeof($textos))
					<ul>
						@foreach($textos as $texto)
							<li><a href="atuacao/{{$texto->{Lang::get('base.campos.slug')} }}" @if($texto->{Lang::get('base.campos.slug')} == $slugFiltro) class="ativo" @endif title="{{$texto->{Lang::get('base.campos.titulo')} }}">{{$texto->{Lang::get('base.campos.titulo')} }}</a></li>
						@endforeach
					</ul>
				@endif
				
			</aside>

			<section class="pure-u-1-2 pure-u-tablet-1 pure-u-mobile-1">

				@if($detalhe)

					@if(!$slugFiltro)
						<h1>{{Lang::get('base.secoes.atuacao') }}</h1>
					@else
						<h1>{{$detalhe->{Lang::get('base.campos.titulo')} }}</h1>
					@endif

					<div class="cke">{{$detalhe->{Lang::get('base.campos.texto')} }}</div>

					@if(!$slugFiltro)
						@if(sizeof($textos))
							<ul id="listaAtuacao">
								@foreach($textos as $texto)
									<li><a href="atuacao/{{$texto->{Lang::get('base.campos.slug')} }}" @if($texto->{Lang::get('base.campos.slug')} == $slugFiltro) class="ativo" @endif title="{{$texto->{Lang::get('base.campos.titulo')} }}">{{$texto->{Lang::get('base.campos.titulo')} }}</a></li>
								@endforeach
							</ul>
						@endif
					@endif
				@endif
				
			</section>

			{{$formulario}}

		</div>

	</div>

@stop