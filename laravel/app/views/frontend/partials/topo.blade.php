<div id="topo" class="pure-g">
	
	<div class="pure-u-1-4 pure-u-tablet-1 pure-u-mobile-1">
		<a href="home" id="logo-link" title="{{Lang::get('base.menu.paginaInicial')}}">
			<img src="assets/images/layout/marca-pieron.png" alt="{{Lang::get('base.geral.institutoPieron')}}">
		</a>
	</div>

	<div id="banner-topo" class="pure-u-3-4 pure-u-tablet-1 pure-u-mobile-1">
		@if($bannerTopo)
			<img src="assets/images/imagenstopo/{{$bannerTopo->imagem}}" alt="{{$textoArea}}">
		@endif
	</div>
</div>