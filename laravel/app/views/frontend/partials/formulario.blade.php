<div id="contato-local" class="pure-u-1-4 pure-u-tablet-1 pure-u-mobile-1">
	<form action="" method="post">
		
		@if(isset($solicitarInfo) && $solicitarInfo)
			<h2>{{Lang::get('base.secoes.solicitarInfo')}}</h2>	
		@else
			<h2>{{Lang::get('base.secoes.contato')}}</h2>
		@endif

		<input type="text" name="nome" id="nomeContato" placeholder="{{Lang::get('base.form.nomeCompleto')}}" required>
		<input type="email" name="email" id="emailContato" placeholder="{{Lang::get('base.form.email')}}" required>
		<input type="text" name="telefone" id="telefoneContato" placeholder="{{Lang::get('base.form.telefone')}}">
		@if(isset($camposExtras) && $camposExtras)
			<input type="text" name="empresa" id="empresaContato" placeholder="{{Lang::get('base.form.empresa')}}">
			<div id="selectPlaceholder">
				<select name="assunto" required class="selectOriginalCursos" id="customSelectContato">
					<option value="">assunto - SELECIONE...</option>
						<option value="Consultoria">Consultoria</option>
						<option value="Assessment estratégico">Assessment estratégico</option>
						<option value="Desenvolvimento">Desenvolvimento</option>
						<option value="Análise de perfil">Análise de perfil</option>
						<option value="Coaching Flow">Coaching Flow</option>
						<option value="Plataforma Insights Discovery">Plataforma Insights Discovery</option>
						<option value="Cursos & Treinamentos">Cursos & Treinamentos</option>
						<option value="Venda de materiais">Venda de materiais</option>
						<option value="Outros">Outros</option>
				</select>
			</div>
		@endif
		<input type="hidden" name="rota" id="rotaContato" value="{{Route::currentRouteName()}}">
		<input type="hidden" name="origem" id="origemContato" value="{{Request::url()}}">
		<textarea name="mensagem" id="mensagemContato" placeholder="{{Lang::get('base.form.mensagem')}}"></textarea>
		<input type="submit" value="{{Lang::get('base.geral.enviar')}} &raquo;"  data-enviando="{{Lang::get('base.geral.enviando')}} &raquo;" data-enviar="{{Lang::get('base.geral.enviar')}} &raquo;">
		
	</form>
	<div class="resposta"></div>
</div>