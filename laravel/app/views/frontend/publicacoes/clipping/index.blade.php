@section('conteudo')

	<div class="centro">
		
		{{$topo}}

		<div id="miolo" class="pure-g">
			
			<aside class="pure-u-1-4 pure-u-tablet-1 pure-u-mobile-1">
				
				<h1>{{Lang::get('base.secoes.publicacoes')}}</h1>

				<ul>
					<li><a href="publicacoes/artigos" @if('artigos' == $slugFiltro) class="ativo" @endif title="{{Lang::get('base.publicacoes.artigos') }}">{{Lang::get('base.publicacoes.artigos') }}</a></li>
					<li><a href="publicacoes/clipping" @if('clipping' == $slugFiltro) class="ativo" @endif title="{{Lang::get('base.publicacoes.clipping') }}">{{Lang::get('base.publicacoes.clipping') }}</a></li>
				</ul>
				
			</aside>

			<section class="pure-u-1-2 pure-u-tablet-1 pure-u-mobile-1">

				<h1>{{Lang::get('base.publicacoes.clipping') }}</h1>

				@if(sizeof($clippings))
					<ul id="listaClipping">
						@foreach($clippings as $clip)
							<li>
								@if($clip->tipo=='galeria')
									<a href='publicacoes/clipping/{{$clip->slug}}' title="{{$clip->{Lang::get('base.campos.titulo')} }}" rel='galeria-{{$clip->id}}' class='shadowGaleria'>
								@elseif($clip->tipo=='link')
									<a href="{{Tools::prepUrl($clip->link)}}" title="{{$clip->{Lang::get('base.campos.titulo')} }}" target='_blank'>
								@else
									<a href="assets/files/clippings/{{$clip->pdf}}" title="{{$clip->{Lang::get('base.campos.titulo')} }}" target='_blank'>
								@endif
									@if($clip->imagem)
										<div class="imagem"><img src="assets/images/clippings/{{$clip->imagem}}" alt="{{$clip->{Lang::get('base.campos.titulo')} }}"></div>
									@endif
									<div class="texto">
										{{$clip->{Lang::get('base.campos.titulo')} }}<br>{{Tools::converteData($clip->data)}}
									</div>
								</a>								
							</li>
						@endforeach
					</ul>
					{{$clippings->links()}}
				@endif
				
			</section>

			{{$formulario}}

		</div>

	</div>

@stop