@section('conteudo')

	<div class="centro">
		
		{{$topo}}

		<div id="miolo" class="pure-g">
			
			<aside class="pure-u-1-4 pure-u-tablet-1 pure-u-mobile-1">
				
				<h1>{{Lang::get('base.secoes.publicacoes')}}</h1>

				<ul>
					<li><a href="publicacoes/artigos" @if('artigos' == $slugFiltro) class="ativo" @endif title="{{Lang::get('base.publicacoes.artigos') }}">{{Lang::get('base.publicacoes.artigos') }}</a></li>
					<li><a href="publicacoes/clipping" @if('clipping' == $slugFiltro) class="ativo" @endif title="{{Lang::get('base.publicacoes.clipping') }}">{{Lang::get('base.publicacoes.clipping') }}</a></li>
				</ul>
				
			</aside>

			<section class="pure-u-1-2 pure-u-tablet-1 pure-u-mobile-1">

				<h1>{{Lang::get('base.publicacoes.artigos') }}</h1>

				@if($artigos)
					<ul id="listaArtigos">
						@foreach($artigos as $artigo)
							<li>
								<a href="publicacoes/artigos/{{$artigo->slug}}" title="{{$artigo->{Lang::get('base.campos.titulo')} }}">
									<h2>{{$artigo->{Lang::get('base.campos.titulo')} }}</h2>
									<h3>{{$artigo->autor.' &middot; '.Tools::dataArtigos($artigo->data)}}</h3>
									<div class="olho">{{$artigo->{Lang::get('base.campos.olho')} }}</div>
									<div class="leiamais">{{Lang::get('base.publicacoes.leiaOArtigoCompleto')}}</div>
								</a>
							</li>
						@endforeach
					</ul>
					{{$artigos->links()}}
				@endif
				
			</section>

			{{$formulario}}

		</div>

	</div>

@stop