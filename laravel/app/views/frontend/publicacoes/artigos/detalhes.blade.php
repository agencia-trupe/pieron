@section('conteudo')

	<div class="centro">
		
		{{$topo}}

		<div id="miolo" class="pure-g">
			
			<aside class="pure-u-1-4 pure-u-tablet-1 pure-u-mobile-1">
				
				<h1>{{Lang::get('base.secoes.publicacoes')}}</h1>

				<ul>
					<li><a href="publicacoes/artigos" @if('artigos' == $slugFiltro) class="ativo" @endif title="{{Lang::get('base.publicacoes.artigos') }}">{{Lang::get('base.publicacoes.artigos') }}</a></li>
					<li><a href="publicacoes/clipping" @if('clipping' == $slugFiltro) class="ativo" @endif title="{{Lang::get('base.publicacoes.clipping') }}">{{Lang::get('base.publicacoes.clipping') }}</a></li>
				</ul>

				@if(sizeof($artigos))
					<h3>{{Lang::get('base.publicacoes.artigosRecentes')}}</h3>
					<ul id="listaArtigosLateral">
						@foreach($artigos as $artigo)
							<li><a href="publicacoes/artigos/{{$artigo->slug}}" title="{{$artigo->{Lang::get('base.campos.titulo')} }}">{{$artigo->{Lang::get('base.campos.titulo')} }}</a></li>
						@endforeach
					</ul>
					<a href="publicacoes/artigos" class="vertodos" title="{{Lang::get('base.publicacoes.verTodosArtigos')}}">{{Lang::get('base.publicacoes.verTodosArtigos')}}</a>
				@endif
				
			</aside>

			<section class="pure-u-1-2 pure-u-tablet-1 pure-u-mobile-1" id="detalhesArtigo">

				<h1>{{Lang::get('base.publicacoes.artigos') }}</h1>

				@if($detalhe)

					<h2>{{$detalhe->{Lang::get('base.campos.titulo')} }}</h2>	
					<h3>{{$detalhe->autor.' &middot; '.Tools::dataArtigos($detalhe->data)}}</h3>
					<div class="cke">{{$detalhe->{Lang::get('base.campos.texto')} }}</div>

					@if($detalhe->pdf)
						<div class="download">
							<a href="publicacoes/download/{{$detalhe->slug}}" title="{{Lang::get('base.publicacoes.baixarPdf')}}">{{Lang::get('base.publicacoes.baixarPdf')}}</a>
						</div>
					@endif

					<div id="navegacao">
						<div class="links">
							@if($previousArtigo)
								<a class="anterior" href="publicacoes/artigos/{{$previousArtigo->slug}}" title="{{Lang::get('base.publicacoes.artigoAnterior')}}">// {{Lang::get('base.publicacoes.artigoAnterior')}} &laquo;</a>
							@endif
							@if($nextArtigo)
								<a class="proximo" href="publicacoes/artigos/{{$nextArtigo->slug}}" title="{{Lang::get('base.publicacoes.proximoArtigo')}}">// {{Lang::get('base.publicacoes.proximoArtigo')}} &raquo;</a>
							@endif
						</div>
						<a href="publicacoes/artigos" class="vertodos" title="{{Lang::get('base.publicacoes.verTodosArtigos')}}">{{Lang::get('base.publicacoes.verTodosArtigos')}}</a>
					</div>
					
				@endif
				
			</section>

			{{$formulario}}

		</div>

	</div>

@stop