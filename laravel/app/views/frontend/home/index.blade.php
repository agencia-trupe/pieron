@section('conteudo')
	
	<a href="home" id="logo-link" class="logo-ajustavel" title="{{Lang::get('base.menu.paginaInicial')}}">
		<img src="assets/images/layout/marca-pieron.png" alt="{{Lang::get('base.geral.institutoPieron')}}">
	</a>
	
	<div id="banners">
		@if(sizeof($banners))
			@foreach($banners as $banner)
				<div title="{{Str::words($banner->{Lang::get('base.campos.texto')}, 10)}}" class="banner" style="background-image:url('assets/images/banners/{{$banner->imagem}}')">
					<div class="texto">
						<p>
							{{nl2br($banner->{Lang::get('base.campos.texto')})}}
						</p>
					</div>
				</div>
			@endforeach
		@endif
	</div>

	<div id="chamadas">
		<div class="centro">
			<div class="listaChamadas">
				@if(sizeof($chamadas))
					@foreach($chamadas as $chamada)
						<a href="{{$chamada->link}}" @if(str_is('http://*', $chamada->link)) target="_blank" @endif title="{{$chamada->{Lang::get('base.campos.titulo')} }}">
							<img src="assets/images/chamadas/{{$chamada->imagem}}" alt="{{$chamada->{Lang::get('base.campos.titulo')} }}">
							<div class="overlay"></div>
							<div class="texto">{{$chamada->{Lang::get('base.campos.titulo')} }}</div>
						</a>
					@endforeach
				@endif
			</div>
			<div id="cadastroNewsletter">
				<h3>{{Lang::get('base.geral.cadastreseNews')}}</h3>
				<div class="containerForm">
					<form action="" method="post" id="form-newsletter">
						<input type="text" name="nome" placeholder="{{Lang::get('base.form.nomeCompleto')}}" required id="newsNome">
						<input type="email" name="email" placeholder="{{Lang::get('base.form.email')}}" required id="newsEmail">
						<input type="submit" value="{{Lang::get('base.geral.enviar')}}">
					</form>
					<div class="resposta"></div>
				</div>
			</div>
		</div>
	</div>

@stop
