@section('conteudo')

    <div class="container add">

      	<h2>
        	Visualizar Contato
        </h2>  

		<div class="pad">

	    	@if(Session::has('sucesso'))
	    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
	        @endif

	    	@if($errors->any())
	    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
	    	@endif	
			
			<div class="form-group">
				<label for="inputTítulo">Assunto</label>
				<div class="well">{{$registro->assunto}}</div>
			</div>

			<div class="form-group">
				<label for="inputTítulo">Nome</label>
				<div class="well">{{$registro->nome}}</div>
			</div>

			<div class="form-group">
				<label for="inputTítulo">E-mail</label>
				<div class="well">{{$registro->email}}</div>
			</div>

			<div class="form-group">
				<label for="inputTítulo">Telefone</label>
				<div class="well">{{$registro->telefone}}</div>
			</div>

			<div class="form-group">
				<label for="inputTítulo">Empresa</label>
				<div class="well">{{$registro->empresa}}</div>
			</div>

			<div class="form-group">
				<label for="inputTítulo">Mensagem</label>
				<div class="well">{{$registro->mensagem}}</div>
			</div>

			<div class="form-group">
				<label for="inputTítulo">Data</label>
				<div class="well">{{Tools::converteDatetime($registro->created_at)}}</div>
			</div>


			<a href="{{URL::route('painel.contatos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</div>
		
    </div>
    
@stop