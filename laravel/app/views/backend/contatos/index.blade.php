@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Contatos Enviados pelo Site
    </h2>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
            	<th>Assunto</th>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Data</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{$registro->assunto}}</td>
                <td>{{$registro->nome}}</td>
                <td>{{$registro->email}}</td>
                <td>{{Tools::converteDatetime($registro->created_at)}}</td>
                <td class="crud-actions" style="width:170px;">
                    <a href='{{ URL::route('painel.contatos.edit', $registro->id ) }}' rel='comentarios' class='btn-comments btn btn-primary btn-sm'><span class="glyphicon glyphicon-zoom-in"></span> visualizar</a>
					{{ Form::open(array('route' => array('painel.contatos.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    {{ $registros->links() }}
</div>

@stop