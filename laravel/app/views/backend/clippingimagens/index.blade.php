@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Imagens do Clipping : {{$clipping->titulo}} <a href='{{ URL::route('painel.clippingimagens.create', array('clipping_id' => $clipping->id)) }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Imagem</a>
    </h2>
	
	<a href="{{URL::route('painel.clippings.index')}}" class="btn btn-sm btn-default" title="Voltar">&larr; voltar</a>
    <hr>


    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='clipping_imagens'>

        <thead>
            <tr>
                <th>Ordenar</th>
				<th>imagem</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @if(sizeof($clipping->imagens))
            @foreach ($clipping->imagens as $registro)

                <tr class="tr-row" id="row_{{ $registro->id }}">
                    <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
    				<td><img src='assets/images/clippingimagens/{{ $registro->imagem }}' style='max-width:150px'></td>
                    <td class="crud-actions">
                        {{ Form::open(array('route' => array('painel.clippingimagens.destroy', $registro->id), 'method' => 'delete')) }}
							<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
						{{ Form::close() }}                    
                    </td>
                </tr>

            @endforeach
        @endif
        </tbody>

    </table>

    
</div>

@stop