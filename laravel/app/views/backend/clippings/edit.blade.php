@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Clipping
        </h2>  

		{{ Form::open( array('route' => array('painel.clippings.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					@if($registro->imagem)
						Imagem Atual<br>
						<img src="assets/images/clippings/{{$registro->imagem}}"><br>
					@endif
					<label for="inputImagem">Trocar Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem">
				</div>
				
				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputTítulo em inglês">Título em inglês</label>
					<input type="text" class="form-control" id="inputTítulo em inglês" name="titulo_us" value="{{$registro->titulo_us}}" >
				</div>

				<div class="form-group">
					<label for="inputData de Publicação">Data de Publicação</label>
					<input type="text" class="form-control datepicker" id="inputData de Publicação" name="data" value="{{ Tools::converteData($registro->data) }}" required>
				</div>

				<label>Tipo de clipping</label>

				<div class="well">					
				
					<div class="form-group">
						<label><input @if($registro->tipo == 'link') checked @endif type="radio" name="tipoClipping" value="link" required> Link</label>
						<div class="inputescondido @if($registro->tipo != 'link') hidden @endif">
							<input type="text" class="form-control" id="inputLink" name="link" value="{{$registro->link}}">
						</div>
					</div>
					
					<div class="form-group">
						<label><input @if($registro->tipo == 'pdf') checked @endif type="radio" name="tipoClipping" value="pdf"> Arquivo PDF</label>
						<div class="inputescondido @if($registro->tipo != 'pdf') hidden @endif">
							@if($registro->pdf)
								Arquivo Atual: <a href="assets/files/clippings/{{$registro->pdf}}" target="_blank">{{$registro->pdf}}</a><br>
							@endif
							<input type="file" class="form-control" id="inputArquivo PDF" name="pdf">						
						</div>
					</div>

					<div class="form-group">
						<label><input @if($registro->tipo == 'galeria') checked @endif type="radio" name="tipoClipping" value="galeria"> Galeria de imagens</label>
						<div class="inputescondido @if($registro->tipo != 'galeria') hidden @endif">
							<div class="alert alert-block alert-warning">
								O botão para gerenciar a galeria de imagens estará disponível após a edição do Clipping.
							</div>
						</div>
					</div>
				
				</div>
				

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.clippings.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop