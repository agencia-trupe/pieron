@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Clippings <a href='{{ URL::route('painel.clippings.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Clipping</a>
    </h2>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Imagem</th>
				<th>Título</th>
				<th>Data de Publicação</th>
                <th>Galeria de Imagens</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td><img src='assets/images/clippings/{{ $registro->imagem }}' style='max-width:150px'></td>
				<td>{{ $registro->titulo }}</td>
				<td>{{ Tools::converteData($registro->data) }}</td>
                <td>
                    @if($registro->tipo == 'galeria')
                        <a href="{{URL::route('painel.clippingimagens.index', array('clipping_id' => $registro->id))}}" class="btn btn-sm btn-info" title="Gerenciar">gerenciar</a>
                    @else
                        <button type="button" class="btn btn-sm btn-info" id="btnGaleriaOff" disabled="disabled">gerenciar</button>
                    @endif
                </td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.clippings.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
					{{ Form::open(array('route' => array('painel.clippings.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    {{ $registros->links() }}
</div>

@stop