@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Clipping
        </h2>  

		<form action="{{URL::route('painel.clippings.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputImagem">Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem" >
				</div>
				
				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['titulo'] }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputTítulo em inglês">Título em inglês</label>
					<input type="text" class="form-control" id="inputTítulo em inglês" name="titulo_us" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['titulo_us'] }}" @endif >
				</div>
				
				<div class="form-group">
					<label for="inputData de Publicação">Data de Publicação</label>
					<input type="text" class="form-control datepicker" id="inputData de Publicação" name="data" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['data'] }}" @endif required>
				</div>

				<label>Tipo de clipping</label>

				<div class="well">
				
					<div class="form-group">
						<label><input type="radio" name="tipoClipping" value="link"> Link</label>
						<div class="inputescondido hidden">
							<input type="text" class="form-control" id="inputLink" name="link" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['link'] }}" @endif>
						</div>
					</div>
					
					<div class="form-group">
						<label><input type="radio" name="tipoClipping" value="pdf"> Arquivo PDF</label>
						<div class="inputescondido hidden">
							<input type="file" class="form-control" id="inputArquivo PDF" name="pdf">
						</div>
					</div>

					<div class="form-group">
						<label><input type="radio" name="tipoClipping" value="galeria"> Galeria de imagens</label>
						<div class="inputescondido hidden">
							<div class="alert alert-block alert-warning">
								O botão para gerenciar a galeria de imagens estará disponível após a inclusão do Clipping.
							</div>
						</div>
					</div>

				</div>

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.clippings.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop