@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Texto
        </h2>  

		<form action="{{URL::route('painel.flowconditionssystem.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

				<div class="form-group large">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control" id="inputTexto" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['texto'] }} @endif</textarea>
				</div>

				<div class="form-group large">
					<label for="inputTextoUS">Texto em inglês</label>
					<textarea name="texto_us" class="form-control" id="inputTextoUS" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['texto_us'] }} @endif</textarea>
				</div>
				

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.flowconditionssystem.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop