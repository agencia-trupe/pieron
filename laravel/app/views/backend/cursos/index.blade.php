@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Cursos <a href='{{ URL::route('painel.cursos.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Curso</a>
    </h2>

    <hr>

    Filtrar por Área :<br>
    @if(sizeof($listaAreas))
        <div class="btn-group" style="margin-top:10px;">
            @foreach($listaAreas as $area)
                <a href="{{URL::route('painel.cursos.index', array('cursos_areas_id' => $area->id))}}" class="btn btn-sm btn-default @if($area->id == $filtroArea) btn-warning @endif">{{$area->titulo}}</a>
            @endforeach
        </div>
    @endif

    <hr>

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='cursos'>

        <thead>
            <tr>
                <th>Ordenar</th>
				<th>Título</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @if(sizeof($registros))
            @foreach ($registros as $registro)

                <tr class="tr-row" id="row_{{ $registro->id }}">
                    <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
    				<td>{{ $registro->titulo }}</td>
                    <td class="crud-actions">
                        <a href='{{ URL::route('painel.cursos.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
    						{{ Form::open(array('route' => array('painel.cursos.destroy', $registro->id), 'method' => 'delete')) }}
    							<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
    						{{ Form::close() }}                    
                    </td>
                </tr>

            @endforeach
        @else
            <tr>
                <td colspan="3" style="text-align:center;">
                    <h4>
                        @if($filtroArea)
                            Nenhum Curso Encontrado
                        @else
                            Selecione uma Área
                        @endif
                    </h4>
                </td>
            </tr>
        @endif
        </tbody>

    </table>

    
</div>

@stop