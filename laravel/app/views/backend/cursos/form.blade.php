@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Curso
        </h2>  

		<form action="{{URL::route('painel.cursos.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

				<div class="form-group">
					<label for="inputTítulo">Nome do Curso</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['titulo'] }}" @endif required>
				</div>

				<div class="form-group">
					<label for="inputÁrea do Curso">Área do Curso</label>					 
					<select	name="cursos_areas_id" class="form-control" id="inputÁrea do Curso" required>
						<option value="">Selecione</option>
						@if(sizeof($listaAreas) > 0)
							@foreach($listaAreas as $area)
								<option value="{{$area->id}}">{{$area->titulo}}</option>
							@endforeach
						@endif
					</select>
				</div>				
		
				<div class="form-group large">
					<label for="inputObjetivos">Objetivos</label>
					<textarea name="objetivos" class="form-control" id="inputObjetivos" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['objetivos'] }} @endif</textarea>
				</div>

				
				
				<div class="form-group large">
					<label for="inputA quem se destina">Público alvo</label>
					<textarea name="publico_alvo" class="form-control" id="inputA quem se destina" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['publico_alvo'] }} @endif</textarea>
				</div>

				
				
				<div class="form-group large">
					<label for="inputCarga Horária">Carga Horária</label>
					<textarea name="carga_horaria" class="form-control" id="inputCarga Horária" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['carga_horaria'] }} @endif</textarea>
				</div>

				
				
				<div class="form-group large">
					<label for="inputMetodologia">Metodologia</label>
					<textarea name="metodologia" class="form-control" id="inputMetodologia" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['metodologia'] }} @endif</textarea>
				</div>

				
				
				<div class="form-group large">
					<label for="inputConteúdo Programático">Conteúdo Programático</label>
					<textarea name="conteudo" class="form-control" id="inputConteúdo Programático" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['conteudo'] }} @endif</textarea>
				</div>

								
				<div class="form-group large">
					<label for="inputDocentes">Docentes</label>
					<textarea name="docentes" class="form-control" id="inputDocentes" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['docentes'] }} @endif</textarea>
				</div>

				
				<div class="form-group large">
					<label for="inputPreReq">Pré-requisitos</label>
					<textarea name="pre_requisitos" class="form-control" id="inputPreReq" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['pre_requisitos'] }} @endif</textarea>
				</div>

								
				<div class="form-group large">
					<label for="inputValor">Valor</label>
					<textarea name="valor" class="form-control" id="inputValor" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['valor'] }} @endif</textarea>
				</div>

								
				<div class="form-group large">
					<label for="inputFormas de Pagamento">Formas de Pagamento</label>
					<textarea name="formas_pagamento" class="form-control" id="inputFormas de Pagamento" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['formas_pagamento'] }} @endif</textarea>
				</div>

								
				<div class="form-group large">
					<label for="inputDocumentacao">Documentação necessária</label>
					<textarea name="documentacao" class="form-control" id="inputDocumentacao" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['documentacao'] }} @endif</textarea>
				</div>

				
				<div class="form-group large">
					<label for="inputObs">Observações</label>
					<textarea name="observacoes" class="form-control" id="inputObs" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['observacoes'] }} @endif</textarea>
				</div>

				
				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.cursos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop