@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Curso
        </h2>  

		{{ Form::open( array('route' => array('painel.cursos.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	

				
				<div class="form-group">
					<label for="inputTítulo">Nome do Curso</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>

				
				<div class="form-group">
					<label for="inputÁrea do Curso">Área do Curso</label>					 
					<select	name="cursos_areas_id" class="form-control" id="inputÁrea do Curso" required>
						<option value="">Selecione</option>
						@if(sizeof($listaAreas) > 0)
							@foreach($listaAreas as $area)
								<option value="{{$area->id}}" @if($registro->cursos_areas_id == $area->id) selected @endif>{{$area->titulo}}</option>
							@endforeach
						@endif
					</select>
				</div>
				
				<div class="form-group large">
					<label for="inputObjetivos">Objetivos</label>
					<textarea name="objetivos" class="form-control" id="inputObjetivos" >{{$registro->objetivos }}</textarea>
				</div>

								
				<div class="form-group large">
					<label for="inputA quem se destina">Público alvo</label>
					<textarea name="publico_alvo" class="form-control" id="inputA quem se destina" >{{$registro->publico_alvo }}</textarea>
				</div>

								
				<div class="form-group large">
					<label for="inputCarga Horária">Carga Horária</label>
					<textarea name="carga_horaria" class="form-control" id="inputCarga Horária" >{{$registro->carga_horaria }}</textarea>
				</div>

								
				<div class="form-group large">
					<label for="inputMetodologia">Metodologia</label>
					<textarea name="metodologia" class="form-control" id="inputMetodologia" >{{$registro->metodologia }}</textarea>
				</div>

							
				<div class="form-group large">
					<label for="inputConteúdo Programático">Conteúdo Programático</label>
					<textarea name="conteudo" class="form-control" id="inputConteúdo Programático" >{{$registro->conteudo }}</textarea>
				</div>

				
				<div class="form-group large">
					<label for="inputDocentes">Docentes</label>
					<textarea name="docentes" class="form-control" id="inputDocentes" >{{$registro->docentes }}</textarea>
				</div>

								
				<div class="form-group large">
					<label for="inputPreReq">Pré-requisitos</label>
					<textarea name="pre_requisitos" class="form-control" id="inputPreReq" >{{ $registro->pre_requisitos }}</textarea>
				</div>

				
				<div class="form-group large">
					<label for="inputValor">Valor</label>
					<textarea name="valor" class="form-control" id="inputValor" >{{$registro->valor }}</textarea>
				</div>

				
				<div class="form-group large">
					<label for="inputFormas de Pagamento">Formas de Pagamento</label>
					<textarea name="formas_pagamento" class="form-control" id="inputFormas de Pagamento" >{{$registro->formas_pagamento }}</textarea>
				</div>

								
				<div class="form-group large">
					<label for="inputDocumentacao">Documentação necessária</label>
					<textarea name="documentacao" class="form-control" id="inputDocumentacao" >{{$registro->documentacao }}</textarea>
				</div>

				
				<div class="form-group large">
					<label for="inputObs">Observações</label>
					<textarea name="observacoes" class="form-control" id="inputObs" >{{$registro->observacoes }}</textarea>
				</div>

				
				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.cursos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop