@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Turmas <a href='{{ URL::route('painel.turmas.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Turma</a>
    </h2>

    <div class="btn-group">
        <a href="{{URL::route('painel.turmas.index', array('unidade' => '1'))}}" title="Turmas de São Paulo" class="btn btn-sm btn-default @if($filtro == '1') btn-info @endif">São Paulo</a>
        <a href="{{URL::route('painel.turmas.index', array('unidade' => '2'))}}" title="Turmas de Campinas" class="btn btn-sm btn-default @if($filtro == '2') btn-info @endif">Campinas</a>
    </div>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
				<th>Curso</th>
				<th>Unidade</th>
                <th>Data de Início</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
				<td>{{ sizeof(\Cursos::find($registro->curso_id)) ? \Cursos::find($registro->curso_id)->titulo : 'Não encontrado'}}</td>
				<td>{{ sizeof(\Unidade::find($registro->unidade_id)) ? \Unidade::find($registro->unidade_id)->cidade : 'Não encontrado' }}</td>
                <td>{{ Tools::converteData($registro->data_inicio) }}</td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.turmas.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
					{{ Form::open(array('route' => array('painel.turmas.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    {{ $registros->appends(array('unidade' => $filtro))->links() }}
</div>

@stop