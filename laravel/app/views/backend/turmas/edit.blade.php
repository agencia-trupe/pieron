@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Turma
        </h2>  

		{{ Form::open( array('route' => array('painel.turmas.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputCurso">Curso</label>					 
					<select	name="curso_id" class="form-control" id="inputCurso" required>
						<option value="">Selecione</option>
						@if(sizeof($listaAreas) > 0)
							@foreach($listaAreas as $area)
								<optgroup label="{{$area->titulo}}">
									@if(sizeof($area->cursosPorNome))
										@foreach($area->cursosPorNome as $curso)
											<option value="{{$curso->id}}" @if($registro->curso_id == $curso->id) selected @endif>{{$curso->titulo}}</option>
										@endforeach
									@endif
								</optgroup>
							@endforeach
						@endif
					</select>
				</div>
				
				<div class="form-group">
					<label for="inputUnidade">Unidade</label>					 
					<select	name="unidade_id" class="form-control" id="inputUnidade" required>
						<option value="">Selecione</option>
						@if(sizeof($listaUnidades) > 0)
							@foreach($listaUnidades as $area)
								<option value="{{$area->id}}" @if($registro->unidade_id == $area->id) selected @endif>{{$area->cidade}}</option>
							@endforeach
						@endif
					</select>
				</div>

				<div class="form-group">
					<label for="inputData de Início">Data de Início</label>
					<input type="text" class="form-control datepicker" id="inputData de Início" name="data_inicio" value="{{ Tools::converteData($registro->data_inicio) }}">
				</div>
				
				<div class="form-group">
					<label for="inputCronograma">Cronograma</label>
					<textarea name="cronograma" class="form-control simpletextarea" id="inputCronograma" >{{$registro->cronograma}}</textarea>
				</div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.turmas.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop