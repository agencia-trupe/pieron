@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Turma
        </h2>  

		<form action="{{URL::route('painel.turmas.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputCurso">Curso</label>
					<select	name="curso_id" class="form-control" id="inputCurso" required>
						<option value="">Selecione</option>
						@if(sizeof($listaAreas) > 0)
							@foreach($listaAreas as $area)
								<optgroup label="{{$area->titulo}}">
									@if(sizeof($area->cursosPorNome))
										@foreach($area->cursosPorNome as $curso)
											<option value="{{$curso->id}}">{{$curso->titulo}}</option>
										@endforeach
									@endif
								</optgroup>
							@endforeach
						@endif
					</select>					
				</div>
				
				<div class="form-group">
					<label for="inputUnidade">Unidade</label>					 
					<select	name="unidade_id" class="form-control" id="inputUnidade" required>
						<option value="">Selecione</option>
						@if(sizeof($listaUnidades) > 0)
							@foreach($listaUnidades as $unidade)
								<option value="{{$unidade->id}}">{{$unidade->cidade}}</option>
							@endforeach
						@endif
					</select>
				</div>

				<div class="form-group">
					<label for="inputData de Início">Data de Início</label>
					<input type="text" class="form-control datepicker" id="inputData de Início" name="data_inicio" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['data_inicio'] }}" @endif>
				</div>
				
				<div class="form-group">
					<label for="inputCronograma">Cronograma</label>
					<textarea name="cronograma" class="form-control simpletextarea" id="inputCronograma" >@if(Session::has('formulario')) <?php $s=Session::get('formulario'); echo $s['cronograma'] ?> @endif</textarea>
				</div>

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.turmas.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop