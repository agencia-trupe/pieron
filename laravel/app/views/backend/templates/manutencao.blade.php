<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, nofollow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    
    <title>Pieron - Painel Administrativo</title>
	
	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>

	<?=Assets::CSS(array(
		'vendor/bootstrap/dist/css/bootstrap.min',		
	))?>

	<?=Assets::JS(array('vendor/modernizr/modernizr'))?>
	
</head>
	<body>
		
		<div class="container" style="margin-top:30px;">
			<div class="panel panel-default">
				<div class="panel-heading">
			    	<h3 class="panel-title">EM MANUTENÇÃO</h3>
				</div>
			  	<div class="panel-body">
			    	<p>
			    		O Painel de administração do site está em manutenção para instalação dos módulos:
			    	</p>
			    	<ul class="list-group">
			    		<li class="list-group-item">Versão do site em inglês</li>
			    		<li class="list-group-item">Layout Responsivo</li>
			    		<li class="list-group-item">Novo sistema para listagem de cursos</li>
			    	</ul>			    	
			  	</div>
			</div>
		</div>

	</body>
</html>
