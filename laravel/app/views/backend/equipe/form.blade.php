@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Membro da Equipe
        </h2>  

		<form action="{{URL::route('painel.equipe.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputNome">Nome</label>
					<input type="text" class="form-control" id="inputNome" name="nome" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['nome'] }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputCargo">Cargo</label>
					<input type="text" class="form-control" id="inputCargo" name="cargo" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['cargo'] }}" @endif >
				</div>

				<div class="form-group">
					<label for="inputCargoUS">Cargo em inglês</label>
					<input type="text" class="form-control" id="inputCargoUS" name="cargo_us" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['cargo_us'] }}" @endif >
				</div>
				
				<div class="form-group large">
					<label for="inputTextodeEntrada">Texto de Entrada</label>
					<textarea name="olho" class="form-control" id="inputTextodeEntrada" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['olho'] }} @endif</textarea>
				</div>

				<div class="form-group large">
					<label for="inputTextodeEntradaUS">Texto de Entrada em inglês</label>
					<textarea name="olho_us" class="form-control" id="inputTextodeEntradaUS" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['olho_us'] }} @endif</textarea>
				</div>
				
				<div class="form-group large">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control" id="inputTexto" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['texto'] }} @endif</textarea>
				</div>

				<div class="form-group large">
					<label for="inputTextoUS">Texto em inglês</label>
					<textarea name="texto_us" class="form-control" id="inputTextoUS" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['texto_us'] }} @endif</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputImagem">Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem" >
				</div>
				

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.equipe.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop