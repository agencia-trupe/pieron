@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Membro da Equipe
        </h2>  

		{{ Form::open( array('route' => array('painel.equipe.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputNome">Nome</label>
					<input type="text" class="form-control" id="inputNome" name="nome" value="{{$registro->nome}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputCargo">Cargo</label>
					<input type="text" class="form-control" id="inputCargo" name="cargo" value="{{$registro->cargo}}" >
				</div>

				<div class="form-group">
					<label for="inputCargoUS">Cargo em inglês</label>
					<input type="text" class="form-control" id="inputCargoUS" name="cargo_us" value="{{$registro->cargo_us}}" >
				</div>
				
				<div class="form-group large">
					<label for="inputTexto de Entrada">Texto de Entrada</label>
					<textarea name="olho" class="form-control" id="inputTexto de Entrada" >{{$registro->olho }}</textarea>
				</div>

				<div class="form-group large">
					<label for="inputTextodeEntradaUS">Texto de Entrada em inglês</label>
					<textarea name="olho_us" class="form-control" id="inputTextodeEntradaUS" >{{$registro->olho_us }}</textarea>
				</div>
				
				<div class="form-group large">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control" id="inputTexto" >{{$registro->texto }}</textarea>
				</div>

				<div class="form-group large">
					<label for="inputTextoUS">Texto em inglês</label>
					<textarea name="texto_us" class="form-control" id="inputTextoUS" >{{$registro->texto_us }}</textarea>
				</div>
				
				<div class="form-group">
					@if($registro->imagem)
						Imagem Atual<br>
						<img src="assets/images/equipe/{{$registro->imagem}}"><br>
					@endif
					<label for="inputImagem">Trocar Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem">
				</div>
				

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.equipe.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop