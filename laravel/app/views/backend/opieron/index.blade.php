@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        O Pieron <a href='{{ URL::route('painel.opieron.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Texto</a>
    </h2>

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='empresa_textos'>

        <thead>
            <tr>
                <th>Ordenar</th>
				<th>Título</th>
				<th>Texto</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
				<td>{{ $registro->titulo }}</td>
				<td>{{ Str::words(strip_tags($registro->texto), 15) }}</td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.opieron.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
                    @if($registro->slug != 'proposito' && $registro->slug != 'como-trabalhamos' && $registro->slug != 'historia' && $registro->slug != 'equipe' && $registro->slug != 'internacional')
					{{ Form::open(array('route' => array('painel.opieron.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}
                    @endif
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop