@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Unidades
    </h2>

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='unidades'>

        <thead>
            <tr>
                <th>Ordenar</th>
				<th>Endereço</th>
				<th>Telefone</th>
				<th>Email</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
				<td>
                    {{ $registro->cidade.'/'.$registro->estado }}<br>
                    {{ $registro->endereco.' '.$registro->cep.' '.$registro->bairro }}<br>
				    {{ $registro->pais }}
                </td>
				<td>{{ $registro->telefone }}</td>
				<td>{{ $registro->email }}</td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.unidades.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
					{{ Form::open(array('route' => array('painel.unidades.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop