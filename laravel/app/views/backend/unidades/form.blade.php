@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Unidade
        </h2>  

		<form action="{{URL::route('painel.unidades.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputCidade">Cidade</label>
					<input type="text" class="form-control" id="inputCidade" name="cidade" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['cidade'] }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputEstado">Estado</label>
					<input type="text" maxlength="2" class="form-control" id="inputEstado" name="estado" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['estado'] }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputPaís">País</label>
					<input type="text" class="form-control" id="inputPaís" name="pais" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['pais'] }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputTelefone">Telefone</label>
					<input type="text" class="form-control" id="inputTelefone" name="telefone" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['telefone'] }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputEmail">Email</label>
					<input type="text" class="form-control" id="inputEmail" name="email" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['email'] }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputEndereço">Endereço</label>
					<input type="text" class="form-control" id="inputEndereço" name="endereco" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['endereco'] }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputBairro">Bairro</label>
					<input type="text" class="form-control" id="inputBairro" name="bairro" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['bairro'] }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputCEP">CEP</label>
					<input type="text" class="form-control" id="inputCEP" name="cep" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['cep'] }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputGoogle Maps">Google Maps</label>
					<input type="text" class="form-control" id="inputGoogle Maps" name="google_maps" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['google_maps'] }}" @endif >
				</div>
				

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.unidades.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop