@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Unidade
        </h2>  

		{{ Form::open( array('route' => array('painel.unidades.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputCidade">Cidade</label>
					<input type="text" class="form-control" disabled id="inputCidade" name="cidade" value="{{$registro->cidade}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputEstado">Estado</label>
					<input type="text" class="form-control" disabled id="inputEstado" name="estado" value="{{$registro->estado}}" required maxlength="2">
				</div>
				
				<div class="form-group">
					<label for="inputPaís">País</label>
					<input type="text" class="form-control" disabled id="inputPaís" name="pais" value="{{$registro->pais}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputTelefone">Telefone</label>
					<input type="text" class="form-control" id="inputTelefone" name="telefone" value="{{$registro->telefone}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputEmail">Email</label>
					<input type="text" class="form-control" id="inputEmail" name="email" value="{{$registro->email}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputEndereço">Endereço</label>
					<input type="text" class="form-control" id="inputEndereço" name="endereco" value="{{$registro->endereco}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputBairro">Bairro</label>
					<input type="text" class="form-control" id="inputBairro" name="bairro" value="{{$registro->bairro}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputCEP">CEP</label>
					<input type="text" class="form-control" id="inputCEP" name="cep" value="{{$registro->cep}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputGoogleMaps">Código de Incorporação do Google Maps</label>
					<input type="text" class="form-control" id="inputGoogleMaps" name="google_maps" value='{{$registro->google_maps}}' >
				</div>
				

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.unidades.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop