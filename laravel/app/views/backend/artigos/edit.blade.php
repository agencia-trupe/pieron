@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Artigo
        </h2>  

		{{ Form::open( array('route' => array('painel.artigos.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputTítulo em inglês">Título em inglês</label>
					<input type="text" class="form-control" id="inputTítulo em inglês" name="titulo_us" value="{{$registro->titulo_us}}" >
				</div>
				
				<div class="form-group">
					<label for="inputAutor">Autor</label>
					<input type="text" class="form-control" id="inputAutor" name="autor" value="{{$registro->autor}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputData de Publicação">Data de Publicação</label>
					<input type="text" class="form-control datepicker" id="inputData de Publicação" name="data" value="{{ Tools::converteData($registro->data) }}" required>
				</div>
				
				<div class="form-group">
					<label for="inputOlho">Olho</label>
					<textarea name="olho" class="form-control" id="inputOlho" >{{$registro->olho }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputOlho em inglês">Olho em inglês</label>
					<textarea name="olho_us" class="form-control" id="inputOlho em inglês" >{{$registro->olho_us }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control" id="inputTexto" >{{$registro->texto }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputTexto em inglês">Texto em inglês</label>
					<textarea name="texto_us" class="form-control" id="inputTexto em inglês" >{{$registro->texto_us }}</textarea>
				</div>

				<div class="form-group">
					@if($registro->pdf)
						<div class="well">	
							Arquivo Atual: <a href="assets/files/artigos/{{$registro->pdf}}" title="Ver arquivo" target="_blank">{{$registro->pdf}}</a><br>
							<label for="inputImagem">Trocar Arquivo PDF</label>
							<input type="file" class="form-control" id="inputImagem" name="pdf"><br><br>
							<label><input type="checkbox" name="remover_arquivo" value="1"> Não disponibilizar nenhum arquivo</label>
						</div>
					@else
						<label for="inputArquivo">Arquivo PDF</label>
						<input type="file" class="form-control" id="inputArquivo" name="pdf">
					@endif
				</div>
				
				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.artigos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop