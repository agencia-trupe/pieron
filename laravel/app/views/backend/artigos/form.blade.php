@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Artigo
        </h2>  

		<form action="{{URL::route('painel.artigos.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['titulo'] }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputTítulo em inglês">Título em inglês</label>
					<input type="text" class="form-control" id="inputTítulo em inglês" name="titulo_us" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['titulo_us'] }}" @endif >
				</div>
				
				<div class="form-group">
					<label for="inputAutor">Autor</label>
					<input type="text" class="form-control" id="inputAutor" name="autor" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['autor'] }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputData de Publicação">Data de Publicação</label>
					<input type="text" class="form-control datepicker" id="inputData de Publicação" name="data" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['data'] }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputOlho">Olho</label>
					<textarea name="olho" class="form-control" id="inputOlho" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['olho'] }} @endif</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputOlho em inglês">Olho em inglês</label>
					<textarea name="olho_us" class="form-control" id="inputOlho em inglês" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['olho_us'] }} @endif</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control" id="inputTexto" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['texto'] }} @endif</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputTexto em inglês">Texto em inglês</label>
					<textarea name="texto_us" class="form-control" id="inputTexto em inglês" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['texto_us'] }} @endif</textarea>
				</div>

				<div class="form-group">
					<label for="inputArquivo">Arquivo PDF</label>
					<input type="file" class="form-control" id="inputArquivo" name="pdf">
				</div>
				

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.artigos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop