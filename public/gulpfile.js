var gulp = require('gulp'); 

// PLUGINS
var imagemin = require('gulp-imagemin');
var pngcrush = require('imagemin-pngcrush');
var cssmin = require('gulp-cssmin');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var clean = require('gulp-clean');

// TASKS

// Otimização de Imagens
gulp.task('optimage', function () {
     return gulp.src('assets/images/layout/*')
		        .pipe(imagemin({
		            progressive: true,
		            svgoPlugins: [{removeViewBox: false}],
		            use: [pngcrush()]
		        }))
		        .pipe(gulp.dest('assets/images/layout/'));
});

// CSS Minify
gulp.task('cssmin', function () {
     return gulp.src('assets/css/*.css')
		        .pipe(cssmin())
		        .pipe(gulp.dest('assets/css/'));
});

// LINT
gulp.task('lint', function() {
    return gulp.src('assets/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// JS Minify
gulp.task('clean-minified', function(){
	return gulp.src('assets/js/*.min.js', {read: false})
				.pipe(clean());
});

gulp.task('clean-minified-painel', function(){
	return gulp.src('assets/js/painel/*.min.js', {read: false})
				.pipe(clean());
});

gulp.task('uglify', function(){
	return gulp.src('assets/js/*.js')
				.pipe(rename(function (path) {
					path.basename += ".min";
				}))
				.pipe(uglify())
				.pipe(gulp.dest('assets/js/'));
});

gulp.task('uglify-painel', function(){
	return gulp.src('assets/js/painel/*.js')
				.pipe(rename(function (path) {
					path.basename += ".min";
				}))
				.pipe(uglify())
				.pipe(gulp.dest('assets/js/painel/'));
});

// DEFAULT
gulp.task('default', ['optimage', 'cssmin' , 'uglify', 'uglify-painel']);
gulp.task('img', ['optimage']);
gulp.task('css', ['cssmin']);
gulp.task('clean', ['clean-minified', 'clean-minified-painel']);
gulp.task('js', ['uglify', 'uglify-painel']);
gulp.task('js-lint', ['lint']);