jQuery.support.placeholder = (function(){
    var i = document.createElement('input');
    return 'placeholder' in i;
})();

function ajustaAlturaHome(){
	if($('.main-home').length){
		var altura = $(window).height();
		var larguraViewport = $(window).width();
		altura = altura - $('header nav').height();		
		if(larguraViewport > 568)
			$('.main-home').css('height', altura);
	}
}

function ajustaMargemLogo(){
	if($('.logo-ajustavel').length){
		var largura = $(window).width();
		var margem = (largura - 1170) / 2;
		if(margem > 0){
			$('.logo-ajustavel').css('left', margem);
			$('#banners .texto').css('left', margem);
		}else{
			$('.logo-ajustavel').css('left', 0);
			$('#banners .texto').css('left', 0);
		}
	}
}

$('document').ready( function(){

	var temporizador;
	var iniciaTemporizador = function(){
		temporizador = setTimeout( function(){
			$('.submenu').removeClass('aberto').removeClass('aberto-pieron').removeClass('aberto-atuacao');
			$('.comsub').removeClass('ativoTemp');			
		}, 1000);
	};

	$('nav ul li a.comsub').hover(function(){
		var subpieron = $(this).hasClass('sub-pieron');
		var subatuacao = $(this).hasClass('sub-atuacao');

		var subpieronAberto = $('.submenu').hasClass('aberto-pieron');
		var subatuacaoAberto = $('.submenu').hasClass('aberto-atuacao');

		var subcontainerAberto = $('.submenu').hasClass('aberto');
		if(!subcontainerAberto){
			$('.submenu').addClass('aberto');
			if($(this).hasClass('sub-pieron')){
				$('.submenu').addClass('aberto-pieron');
			}else{
				$('.submenu').addClass('aberto-atuacao');
			}
			$(this).addClass('ativoTemp');
			clearTimeout(temporizador);
		}else{
			if((subpieron && subatuacaoAberto) || (subatuacao && subpieronAberto)){
				if($(this).hasClass('sub-pieron')){
					$('.submenu').removeClass('aberto-atuacao').addClass('aberto-pieron');
				}else{
					$('.submenu').removeClass('aberto-pieron').addClass('aberto-atuacao');
				}
				$('.ativoTemp').removeClass('ativoTemp');
				$(this).addClass('ativoTemp');
				clearTimeout(temporizador);
			}
		}
	}, function(){
		iniciaTemporizador();
	});

	$('.submenu').hover( function(){
		clearTimeout(temporizador);
	}, function(){
		iniciaTemporizador();
	});

	ajustaAlturaHome();
	ajustaMargemLogo();

	window.onresize = function(event) {    
		ajustaAlturaHome();
		ajustaMargemLogo();
	};

	if($('#banners').length){
		$('#banners').cycle({
			timeout : 7000,
			random : 1
		});
	}

	$('#form-newsletter').submit( function(e){
		e.preventDefault();
		$.post('cadastroNewsletter', {
			nome : $('#newsNome').val(),
			email : $('#newsEmail').val()
		}, function(resposta){
			$('.resposta').html(resposta);
			$('#newsNome').val('');
			$('#newsEmail').val('');
			$('#form-newsletter').addClass('escondido');
			setTimeout(function(){
				$('.resposta').html('');
				$('#form-newsletter').removeClass('escondido');
			}, 5000);
		});
	});

	$('#contato-local form').submit( function(e){
		e.preventDefault();

		var enviar = $('#contato-local form').find("input[type='submit']");

		if($('#nomeContato').val() === ''){
			alert('Informe seu nome!');
			return false;
		}

		if($('#emailContato').val() === ''){
			alert('Informe seu e-mail!');
			return false;
		}

		if($('#customSelectContato').length && $('#customSelectContato').val() === ''){
			alert('Informe o motivo do contato!');
			return false;
		}

		$('#contato-local form').addClass('enviando');
		enviar.val(enviar.attr('data-enviando'));

		$.post('envioContato', {
			nome : $('#nomeContato').val(),
			email : $('#emailContato').val(),
			telefone : $('#telefoneContato').val(),
			empresa : $('#empresaContato').val(),
			assunto : $('#customSelectContato').val(),
			mensagem : $('#mensagemContato').val(),
			origem : $('#origemContato').val(),
			rota : $('#rotaContato').val()
		}, function(resposta){

			$('.resposta').html(resposta);

			$('#nomeContato').val('');
			$('#emailContato').val('');
			$('#telefoneContato').val('');
			$('#empresaContato').val('');
			$('#customSelectContato').val('');
			$('#origemContato').val('');
			$('#mensagemContato').val('');
			$('#rotaContato').val('');

			$('#contato-local form').addClass('escondido');

			setTimeout(function(){
				$('.resposta').html('');
				enviar.val(enviar.attr('data-enviar'));
				$('#contato-local form').removeClass('enviando').removeClass('escondido');
			}, 5000);
		});
	});

	$('.botao-toggle').click( function(e){
		e.preventDefault();
		var botao = $(this);
		var target = botao.parent().find('.detalhe');
		if(!target.hasClass('aberto')){
			target.addClass('aberto');
			botao.html(botao.attr('data-fechar'));
		}else{
			target.removeClass('aberto');
			botao.html(botao.attr('data-abrir'));
		}
	});

	$('#filtra-cursos').change( function(){
		window.location = 'cursos-e-treinamentos/area/'+$(this).val();
	});

	$('#customSelectContato').customSelect({
    	customClass: "selectContato",
	});

	$('#filtra-cursos').customSelect({
		customClass : 'selectCursos'
	});

	$('.shadowGaleria').fancybox({
		type : 'ajax',
        helpers:  {
			title : {
	            type : 'inside'
	        }
	    }
	});

	$('#scrollTopBtn').click( function(e){
		e.preventDefault();
		$('html, body').animate({scrollTop : 0}, 400);
	});

	$('#mobileAbrirMenu a').click( function(e){
		e.preventDefault();
		$('header nav ul').addClass('mobileAberto');
	});

	if(!$.support.placeholder){
	    $(this).find('[placeholder]').each(function(){
	        if ($(this).val() === ''){
	            $(this).val( $(this).attr('placeholder') );
	        }
	    });

	    $('[placeholder]').focus(function(){
	        if ($(this).val() == $(this).attr('placeholder')){
	            $(this).val('');
	            $(this).removeClass('placeholder');
	        }
	    }).blur(function(){
	        if ($(this).val() === '' || $(this).val() == $(this).attr('placeholder')){
	            $(this).val($(this).attr('placeholder'));
	            $(this).addClass('placeholder');
	        }
	    });

	    $('[placeholder]').closest('form').submit(function(){
	        $(this).find('[placeholder]').each(function(){
	            if ($(this).val() == $(this).attr('placeholder')){
	                $(this).val('');
	            }
	        });
	    });
	}

});
